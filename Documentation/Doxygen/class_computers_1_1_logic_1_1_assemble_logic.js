var class_computers_1_1_logic_1_1_assemble_logic =
[
    [ "AssembleLogic", "class_computers_1_1_logic_1_1_assemble_logic.html#a4307f509ce39a8cec8c0774c6418bd9c", null ],
    [ "ASyncCompatibleProcessorCount", "class_computers_1_1_logic_1_1_assemble_logic.html#a5627792a6fdca4fba7788846e7434aff", null ],
    [ "ChangeMotherboardSocket", "class_computers_1_1_logic_1_1_assemble_logic.html#a9aec86d9f30d3a9095b1abef665f0ac7", null ],
    [ "ChangeProcessorName", "class_computers_1_1_logic_1_1_assemble_logic.html#ad16f73437167211d1be9644e74d9c4f2", null ],
    [ "Compatible", "class_computers_1_1_logic_1_1_assemble_logic.html#a2292c8569038aa50fabfd36b4fa1bf55", null ],
    [ "CompatibleProcessorCount", "class_computers_1_1_logic_1_1_assemble_logic.html#afb9ce4e83c66716f51664922a26ad91f", null ],
    [ "GetAllMotherboard", "class_computers_1_1_logic_1_1_assemble_logic.html#ad83107d9e6780d4646a467e9d5df49a8", null ],
    [ "GetAllProcessor", "class_computers_1_1_logic_1_1_assemble_logic.html#aaffe8e87fcb9e9774354aaa02b74b223", null ],
    [ "GetOneMotherboard", "class_computers_1_1_logic_1_1_assemble_logic.html#afbee4e80db17995bfc04d6a0883a3204", null ],
    [ "GetOneProcessor", "class_computers_1_1_logic_1_1_assemble_logic.html#ad8828896669f9d24b60997466b5723e6", null ],
    [ "OverclockProcessor", "class_computers_1_1_logic_1_1_assemble_logic.html#ab5101237885d642a8935eee75b778a7b", null ],
    [ "RepairMotherboard", "class_computers_1_1_logic_1_1_assemble_logic.html#accbfc8dfacab37a6a915e8fcf1245fdc", null ],
    [ "RepairProcessor", "class_computers_1_1_logic_1_1_assemble_logic.html#a2bc8b91cc86ada92e07290b1ed62cd3e", null ]
];