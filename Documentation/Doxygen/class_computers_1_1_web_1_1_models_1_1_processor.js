var class_computers_1_1_web_1_1_models_1_1_processor =
[
    [ "CompanyName", "class_computers_1_1_web_1_1_models_1_1_processor.html#aee023034a225be52fdb9c9d6b1e55257", null ],
    [ "CoreCount", "class_computers_1_1_web_1_1_models_1_1_processor.html#a727d43e90951fa499a8115768268c6d0", null ],
    [ "Damaged", "class_computers_1_1_web_1_1_models_1_1_processor.html#a439307c02b0af4540e78d2fdffe37c8b", null ],
    [ "Ghz", "class_computers_1_1_web_1_1_models_1_1_processor.html#a5a8813c68812410fc7ebbc56ef94108c", null ],
    [ "Id", "class_computers_1_1_web_1_1_models_1_1_processor.html#a47ddedd0a6c5591f4d9bc93d9fb88e8c", null ],
    [ "Name", "class_computers_1_1_web_1_1_models_1_1_processor.html#aa1b5d5e8fcce29b5af6b3c70cee3827e", null ],
    [ "Price", "class_computers_1_1_web_1_1_models_1_1_processor.html#a1fcf2371d3cbe4d5425e82623155536c", null ],
    [ "Socket", "class_computers_1_1_web_1_1_models_1_1_processor.html#a90233e6675b74d2147a068503962790f", null ]
];