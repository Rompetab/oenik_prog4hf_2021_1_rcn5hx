var namespace_computers_1_1_logic =
[
    [ "Tests", "namespace_computers_1_1_logic_1_1_tests.html", "namespace_computers_1_1_logic_1_1_tests" ],
    [ "AssembleLogic", "class_computers_1_1_logic_1_1_assemble_logic.html", "class_computers_1_1_logic_1_1_assemble_logic" ],
    [ "AvarageProcessorCost", "class_computers_1_1_logic_1_1_avarage_processor_cost.html", "class_computers_1_1_logic_1_1_avarage_processor_cost" ],
    [ "CompanyWorkerCount", "class_computers_1_1_logic_1_1_company_worker_count.html", "class_computers_1_1_logic_1_1_company_worker_count" ],
    [ "CompatibleProcessorCount", "class_computers_1_1_logic_1_1_compatible_processor_count.html", "class_computers_1_1_logic_1_1_compatible_processor_count" ],
    [ "IAssembleLogic", "interface_computers_1_1_logic_1_1_i_assemble_logic.html", "interface_computers_1_1_logic_1_1_i_assemble_logic" ],
    [ "IComputersLogic", "interface_computers_1_1_logic_1_1_i_computers_logic.html", "interface_computers_1_1_logic_1_1_i_computers_logic" ],
    [ "IManufactureLogic", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html", "interface_computers_1_1_logic_1_1_i_manufacture_logic" ],
    [ "ManufactureLogic", "class_computers_1_1_logic_1_1_manufacture_logic.html", "class_computers_1_1_logic_1_1_manufacture_logic" ]
];