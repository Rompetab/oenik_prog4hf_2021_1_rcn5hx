var class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests =
[
    [ "Setup", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#a41ccc1d3d6f9bd2bd119d035e583217d", null ],
    [ "TestAddaNewMotherboard", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#add99df3b0b822895ead6b2bd939c3f98", null ],
    [ "TestAvarageProcessorCost", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#ab0f8a0801662417ff7abb7d44ef4586c", null ],
    [ "TestChangeCompanyLocation", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#ac8359388565def7c4d8845b732006cca", null ],
    [ "TestCompanyWorkerCount", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#a57e7cadb50ea66906217f90562bec71c", null ],
    [ "TestGetAllCompanies", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#a910fdcf0f4825f7514b9292e87061bd4", null ],
    [ "TestGetOneProcessor", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#a0646eb6833e4dcff2f84dfe2648e9a29", null ],
    [ "TestRemoveMotherboard", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html#a7f504210c7ce597713f9f5509062695b", null ]
];