var interface_computers_1_1_logic_1_1_i_manufacture_logic =
[
    [ "AddCompany", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#abdde33fd16c56645c4521709f705c8a0", null ],
    [ "AddMotherboard", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#aec38b2d3af4ddc00d1d9658199cbc810", null ],
    [ "AddProcessor", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#a7c3739e6ecdedd2b23aff5ada50d7a37", null ],
    [ "ChangeCompanyLocation", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#ab8252035a26d8967bd58e93c9173d1be", null ],
    [ "ChangeProcessor", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#abc52993a7e918c368bd3217873bc773f", null ],
    [ "ChangeProcessorPrice", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#ae0da94f738182efc48d072de9c08c576", null ],
    [ "GetAllCompany", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#aae09a4213d74d31d58b93f87dfcb2d1d", null ],
    [ "GetOneCompany", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#a82bc2399ae34214fb43572816e82195f", null ],
    [ "RemoveCompany", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#ac4edfbb8244d021a6bad5cac0673f2da", null ],
    [ "RemoveMotherboard", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#a53089dcf9a57bfc63fe1be50c2e8ec34", null ],
    [ "RemoveProcessor", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html#ae38c8bc0406a94a53e9ae9faedabbc5b", null ]
];