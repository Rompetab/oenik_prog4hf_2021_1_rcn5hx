var interface_computers_1_1_logic_1_1_i_assemble_logic =
[
    [ "ChangeMotherboardSocket", "interface_computers_1_1_logic_1_1_i_assemble_logic.html#a88db8957e96890567c7478804bfb45e5", null ],
    [ "ChangeProcessorName", "interface_computers_1_1_logic_1_1_i_assemble_logic.html#a5494f0ca94da59973dacbf7c2566595d", null ],
    [ "Compatible", "interface_computers_1_1_logic_1_1_i_assemble_logic.html#ab5b1dd73aa64ac02916a290109dc1590", null ],
    [ "OverclockProcessor", "interface_computers_1_1_logic_1_1_i_assemble_logic.html#aa0fbef21e2143a400ee9d0a324911aeb", null ],
    [ "RepairMotherboard", "interface_computers_1_1_logic_1_1_i_assemble_logic.html#ae3dbf6d1d587ff779650e7e5d6ea1458", null ],
    [ "RepairProcessor", "interface_computers_1_1_logic_1_1_i_assemble_logic.html#af1e44480bbd724faf06cb93149898e02", null ]
];