var namespace_computers_1_1_w_p_f_client =
[
    [ "App", "class_computers_1_1_w_p_f_client_1_1_app.html", "class_computers_1_1_w_p_f_client_1_1_app" ],
    [ "EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", "class_computers_1_1_w_p_f_client_1_1_editor_window" ],
    [ "IMainLogic", "interface_computers_1_1_w_p_f_client_1_1_i_main_logic.html", "interface_computers_1_1_w_p_f_client_1_1_i_main_logic" ],
    [ "MainLogic", "class_computers_1_1_w_p_f_client_1_1_main_logic.html", "class_computers_1_1_w_p_f_client_1_1_main_logic" ],
    [ "MainVM", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html", "class_computers_1_1_w_p_f_client_1_1_main_v_m" ],
    [ "MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", "class_computers_1_1_w_p_f_client_1_1_main_window" ],
    [ "ProcessorVM", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html", "class_computers_1_1_w_p_f_client_1_1_processor_v_m" ]
];