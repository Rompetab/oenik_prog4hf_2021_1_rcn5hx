var class_computers_1_1_w_p_f_client_1_1_processor_v_m =
[
    [ "CopyFrom", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#a3b8dc987881250ed8e629097203559fd", null ],
    [ "CompanyName", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#a9428a83a22498f3908eec0faf00addec", null ],
    [ "CoreCount", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#aba58260d12495447f0ce9de25e38f205", null ],
    [ "Damaged", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#a94a58e1fa07d617ce17e57b224395eec", null ],
    [ "Ghz", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#aa330c4c6e0cbdbad8cd69a0e64f80449", null ],
    [ "Id", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#afa9f92ea2212091a6a7a7f094d9f4128", null ],
    [ "Name", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#ac5985066758c60327dd032d730fb91c9", null ],
    [ "Price", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#abda23327b4aa844aa4de27893bb6847c", null ],
    [ "Socket", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#ac95da433f6cd755cd6b2a59f8b465e3e", null ]
];