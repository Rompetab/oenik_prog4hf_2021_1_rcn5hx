var class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a0145c57be205d8f617423f169185c217", null ],
    [ "MainViewModel", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a2a33c9b70cae426155422e8513450a90", null ],
    [ "AddCommand", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a5ddd68661ae50d91a239b98061e17107", null ],
    [ "EditCommand", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a603391234410c9e0252eeb98f6b1e903", null ],
    [ "Processors", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a05d9c9c4639d11d97d070ccf483aca56", null ],
    [ "RemoveCommand", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#ad929c27bb548257bfccae1da3c48c666", null ],
    [ "SelectedProcessor", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#aad153c60d935e9f2c6e0a576e5cb602b", null ]
];