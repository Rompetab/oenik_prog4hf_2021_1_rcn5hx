var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Processors_ProcessorsDetails", "class_asp_net_core_1_1_views___processors___processors_details.html", "class_asp_net_core_1_1_views___processors___processors_details" ],
      [ "Views_Processors_ProcessorsEdit", "class_asp_net_core_1_1_views___processors___processors_edit.html", "class_asp_net_core_1_1_views___processors___processors_edit" ],
      [ "Views_Processors_ProcessorsIndex", "class_asp_net_core_1_1_views___processors___processors_index.html", "class_asp_net_core_1_1_views___processors___processors_index" ],
      [ "Views_Processors_ProcessorsList", "class_asp_net_core_1_1_views___processors___processors_list.html", "class_asp_net_core_1_1_views___processors___processors_list" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "Computers", "namespace_computers.html", [
      [ "Data", "namespace_computers_1_1_data.html", [
        [ "Model", "namespace_computers_1_1_data_1_1_model.html", [
          [ "ComputerContext", "class_computers_1_1_data_1_1_model_1_1_computer_context.html", "class_computers_1_1_data_1_1_model_1_1_computer_context" ]
        ] ],
        [ "Company", "class_computers_1_1_data_1_1_company.html", "class_computers_1_1_data_1_1_company" ],
        [ "Motherboard", "class_computers_1_1_data_1_1_motherboard.html", "class_computers_1_1_data_1_1_motherboard" ],
        [ "Processor", "class_computers_1_1_data_1_1_processor.html", "class_computers_1_1_data_1_1_processor" ]
      ] ],
      [ "Logic", "namespace_computers_1_1_logic.html", [
        [ "Tests", "namespace_computers_1_1_logic_1_1_tests.html", [
          [ "AssembleLogicTests", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests" ],
          [ "ManufactureLogicTests", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests" ]
        ] ],
        [ "AssembleLogic", "class_computers_1_1_logic_1_1_assemble_logic.html", "class_computers_1_1_logic_1_1_assemble_logic" ],
        [ "AvarageProcessorCost", "class_computers_1_1_logic_1_1_avarage_processor_cost.html", "class_computers_1_1_logic_1_1_avarage_processor_cost" ],
        [ "CompanyWorkerCount", "class_computers_1_1_logic_1_1_company_worker_count.html", "class_computers_1_1_logic_1_1_company_worker_count" ],
        [ "CompatibleProcessorCount", "class_computers_1_1_logic_1_1_compatible_processor_count.html", "class_computers_1_1_logic_1_1_compatible_processor_count" ],
        [ "IAssembleLogic", "interface_computers_1_1_logic_1_1_i_assemble_logic.html", "interface_computers_1_1_logic_1_1_i_assemble_logic" ],
        [ "IComputersLogic", "interface_computers_1_1_logic_1_1_i_computers_logic.html", "interface_computers_1_1_logic_1_1_i_computers_logic" ],
        [ "IManufactureLogic", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html", "interface_computers_1_1_logic_1_1_i_manufacture_logic" ],
        [ "ManufactureLogic", "class_computers_1_1_logic_1_1_manufacture_logic.html", "class_computers_1_1_logic_1_1_manufacture_logic" ]
      ] ],
      [ "Program", "namespace_computers_1_1_program.html", [
        [ "Factory", "class_computers_1_1_program_1_1_factory.html", "class_computers_1_1_program_1_1_factory" ],
        [ "MainComputer", "class_computers_1_1_program_1_1_main_computer.html", "class_computers_1_1_program_1_1_main_computer" ]
      ] ],
      [ "Repository", "namespace_computers_1_1_repository.html", [
        [ "CompanyRepository", "class_computers_1_1_repository_1_1_company_repository.html", "class_computers_1_1_repository_1_1_company_repository" ],
        [ "ComputersRepo", "class_computers_1_1_repository_1_1_computers_repo.html", "class_computers_1_1_repository_1_1_computers_repo" ],
        [ "ICompanyRepository", "interface_computers_1_1_repository_1_1_i_company_repository.html", "interface_computers_1_1_repository_1_1_i_company_repository" ],
        [ "IMotherboardRepository", "interface_computers_1_1_repository_1_1_i_motherboard_repository.html", "interface_computers_1_1_repository_1_1_i_motherboard_repository" ],
        [ "IProcessorRepository", "interface_computers_1_1_repository_1_1_i_processor_repository.html", "interface_computers_1_1_repository_1_1_i_processor_repository" ],
        [ "IRepository", "interface_computers_1_1_repository_1_1_i_repository.html", "interface_computers_1_1_repository_1_1_i_repository" ],
        [ "MotherboardRepository", "class_computers_1_1_repository_1_1_motherboard_repository.html", "class_computers_1_1_repository_1_1_motherboard_repository" ],
        [ "ProcessorRepository", "class_computers_1_1_repository_1_1_processor_repository.html", "class_computers_1_1_repository_1_1_processor_repository" ]
      ] ],
      [ "Web", "namespace_computers_1_1_web.html", [
        [ "Controllers", "namespace_computers_1_1_web_1_1_controllers.html", [
          [ "ApiResult", "class_computers_1_1_web_1_1_controllers_1_1_api_result.html", "class_computers_1_1_web_1_1_controllers_1_1_api_result" ],
          [ "HomeController", "class_computers_1_1_web_1_1_controllers_1_1_home_controller.html", "class_computers_1_1_web_1_1_controllers_1_1_home_controller" ],
          [ "ProcessorApiController", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller" ],
          [ "ProcessorsController", "class_computers_1_1_web_1_1_controllers_1_1_processors_controller.html", "class_computers_1_1_web_1_1_controllers_1_1_processors_controller" ]
        ] ],
        [ "Models", "namespace_computers_1_1_web_1_1_models.html", [
          [ "ErrorViewModel", "class_computers_1_1_web_1_1_models_1_1_error_view_model.html", "class_computers_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_computers_1_1_web_1_1_models_1_1_mapper_factory.html", "class_computers_1_1_web_1_1_models_1_1_mapper_factory" ],
          [ "Processor", "class_computers_1_1_web_1_1_models_1_1_processor.html", "class_computers_1_1_web_1_1_models_1_1_processor" ],
          [ "ProcessorListViewModel", "class_computers_1_1_web_1_1_models_1_1_processor_list_view_model.html", "class_computers_1_1_web_1_1_models_1_1_processor_list_view_model" ]
        ] ],
        [ "Program", "class_computers_1_1_web_1_1_program.html", "class_computers_1_1_web_1_1_program" ],
        [ "Startup", "class_computers_1_1_web_1_1_startup.html", "class_computers_1_1_web_1_1_startup" ]
      ] ],
      [ "WPF", "namespace_computers_1_1_w_p_f.html", [
        [ "BL", "namespace_computers_1_1_w_p_f_1_1_b_l.html", [
          [ "IEditorService", "interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", "interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_editor_service" ],
          [ "IProcessorLogic", "interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_processor_logic.html", "interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_processor_logic" ],
          [ "ProcessorLogic", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic" ]
        ] ],
        [ "UI", "namespace_computers_1_1_w_p_f_1_1_u_i.html", [
          [ "ComputerEditorWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window.html", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window" ],
          [ "CompanyToBrushConverter", "class_computers_1_1_w_p_f_1_1_u_i_1_1_company_to_brush_converter.html", "class_computers_1_1_w_p_f_1_1_u_i_1_1_company_to_brush_converter" ],
          [ "EditorViaWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_editor_via_window.html", "class_computers_1_1_w_p_f_1_1_u_i_1_1_editor_via_window" ],
          [ "IntToCompanyIdConverter", "class_computers_1_1_w_p_f_1_1_u_i_1_1_int_to_company_id_converter.html", "class_computers_1_1_w_p_f_1_1_u_i_1_1_int_to_company_id_converter" ],
          [ "IntToCompanyNameConverter", "class_computers_1_1_w_p_f_1_1_u_i_1_1_int_to_company_name_converter.html", "class_computers_1_1_w_p_f_1_1_u_i_1_1_int_to_company_name_converter" ]
        ] ],
        [ "VM", "namespace_computers_1_1_w_p_f_1_1_v_m.html", [
          [ "EditorViewModel", "class_computers_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", "class_computers_1_1_w_p_f_1_1_v_m_1_1_editor_view_model" ],
          [ "MainViewModel", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model" ]
        ] ],
        [ "App", "class_computers_1_1_w_p_f_1_1_app.html", "class_computers_1_1_w_p_f_1_1_app" ],
        [ "MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", "class_computers_1_1_w_p_f_1_1_main_window" ],
        [ "MyIoc", "class_computers_1_1_w_p_f_1_1_my_ioc.html", "class_computers_1_1_w_p_f_1_1_my_ioc" ]
      ] ],
      [ "WPFClient", "namespace_computers_1_1_w_p_f_client.html", [
        [ "App", "class_computers_1_1_w_p_f_client_1_1_app.html", "class_computers_1_1_w_p_f_client_1_1_app" ],
        [ "EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", "class_computers_1_1_w_p_f_client_1_1_editor_window" ],
        [ "IMainLogic", "interface_computers_1_1_w_p_f_client_1_1_i_main_logic.html", "interface_computers_1_1_w_p_f_client_1_1_i_main_logic" ],
        [ "MainLogic", "class_computers_1_1_w_p_f_client_1_1_main_logic.html", "class_computers_1_1_w_p_f_client_1_1_main_logic" ],
        [ "MainVM", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html", "class_computers_1_1_w_p_f_client_1_1_main_v_m" ],
        [ "MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", "class_computers_1_1_w_p_f_client_1_1_main_window" ],
        [ "ProcessorVM", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html", "class_computers_1_1_w_p_f_client_1_1_processor_v_m" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];