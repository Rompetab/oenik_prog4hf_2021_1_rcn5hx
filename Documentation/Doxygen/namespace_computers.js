var namespace_computers =
[
    [ "Data", "namespace_computers_1_1_data.html", "namespace_computers_1_1_data" ],
    [ "Logic", "namespace_computers_1_1_logic.html", "namespace_computers_1_1_logic" ],
    [ "Program", "namespace_computers_1_1_program.html", "namespace_computers_1_1_program" ],
    [ "Repository", "namespace_computers_1_1_repository.html", "namespace_computers_1_1_repository" ],
    [ "Web", "namespace_computers_1_1_web.html", "namespace_computers_1_1_web" ],
    [ "WPF", "namespace_computers_1_1_w_p_f.html", "namespace_computers_1_1_w_p_f" ],
    [ "WPFClient", "namespace_computers_1_1_w_p_f_client.html", "namespace_computers_1_1_w_p_f_client" ]
];