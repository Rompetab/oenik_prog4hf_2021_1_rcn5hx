var class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller =
[
    [ "ProcessorApiController", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html#af2cff44bb260eabf21e14f64dd3059d9", null ],
    [ "AddOneProcessor", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html#a04795adb9b2b9986763f725f9e0966cb", null ],
    [ "DelOneProcessor", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html#aafc27af6f4e03179bf24515f4b95420d", null ],
    [ "GetAll", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html#a0608798f624bae92afe222bb7025fbb2", null ],
    [ "ModOneProcessor", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html#a37646e504c74d48935996e4db4e9b364", null ]
];