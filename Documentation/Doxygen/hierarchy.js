var hierarchy =
[
    [ "Computers.Web.Controllers.ApiResult", "class_computers_1_1_web_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "Computers.WPF.App", "class_computers_1_1_w_p_f_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "Computers.WPF.App", "class_computers_1_1_w_p_f_1_1_app.html", null ],
      [ "Computers.WPF.App", "class_computers_1_1_w_p_f_1_1_app.html", null ],
      [ "Computers.WPF.App", "class_computers_1_1_w_p_f_1_1_app.html", null ],
      [ "Computers.WPFClient.App", "class_computers_1_1_w_p_f_client_1_1_app.html", null ],
      [ "Computers.WPFClient.App", "class_computers_1_1_w_p_f_client_1_1_app.html", null ],
      [ "Computers.WPFClient.App", "class_computers_1_1_w_p_f_client_1_1_app.html", null ],
      [ "Computers.WPFClient.App", "class_computers_1_1_w_p_f_client_1_1_app.html", null ]
    ] ],
    [ "Computers.Logic.Tests.AssembleLogicTests", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html", null ],
    [ "Computers.Logic.AvarageProcessorCost", "class_computers_1_1_logic_1_1_avarage_processor_cost.html", null ],
    [ "Computers.Data.Company", "class_computers_1_1_data_1_1_company.html", null ],
    [ "Computers.Logic.CompanyWorkerCount", "class_computers_1_1_logic_1_1_company_worker_count.html", null ],
    [ "Computers.Logic.CompatibleProcessorCount", "class_computers_1_1_logic_1_1_compatible_processor_count.html", null ],
    [ "Computers.Repository.ComputersRepo< Company >", "class_computers_1_1_repository_1_1_computers_repo.html", [
      [ "Computers.Repository.CompanyRepository", "class_computers_1_1_repository_1_1_company_repository.html", null ]
    ] ],
    [ "Computers.Repository.ComputersRepo< Motherboard >", "class_computers_1_1_repository_1_1_computers_repo.html", [
      [ "Computers.Repository.MotherboardRepository", "class_computers_1_1_repository_1_1_motherboard_repository.html", null ]
    ] ],
    [ "Computers.Repository.ComputersRepo< Processor >", "class_computers_1_1_repository_1_1_computers_repo.html", [
      [ "Computers.Repository.ProcessorRepository", "class_computers_1_1_repository_1_1_processor_repository.html", null ]
    ] ],
    [ "Controller", null, [
      [ "Computers.Web.Controllers.HomeController", "class_computers_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "Computers.Web.Controllers.ProcessorApiController", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html", null ],
      [ "Computers.Web.Controllers.ProcessorsController", "class_computers_1_1_web_1_1_controllers_1_1_processors_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Computers.Data.Model.ComputerContext", "class_computers_1_1_data_1_1_model_1_1_computer_context.html", null ]
    ] ],
    [ "Computers.Web.Models.ErrorViewModel", "class_computers_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "Computers.Program.Factory", "class_computers_1_1_program_1_1_factory.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Computers.WPF.UI.ComputerEditorWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window.html", null ],
      [ "Computers.WPF.UI.ComputerEditorWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window.html", null ],
      [ "Computers.WPFClient.EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", null ],
      [ "Computers.WPFClient.EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", null ],
      [ "Computers.WPFClient.MainLogic", "class_computers_1_1_w_p_f_client_1_1_main_logic.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ]
    ] ],
    [ "Computers.Logic.IComputersLogic", "interface_computers_1_1_logic_1_1_i_computers_logic.html", [
      [ "Computers.Logic.IAssembleLogic", "interface_computers_1_1_logic_1_1_i_assemble_logic.html", [
        [ "Computers.Logic.AssembleLogic", "class_computers_1_1_logic_1_1_assemble_logic.html", null ]
      ] ],
      [ "Computers.Logic.IManufactureLogic", "interface_computers_1_1_logic_1_1_i_manufacture_logic.html", [
        [ "Computers.Logic.ManufactureLogic", "class_computers_1_1_logic_1_1_manufacture_logic.html", null ]
      ] ]
    ] ],
    [ "IDisposable", null, [
      [ "Computers.WPFClient.MainLogic", "class_computers_1_1_w_p_f_client_1_1_main_logic.html", null ]
    ] ],
    [ "Computers.WPF.BL.IEditorService", "interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", [
      [ "Computers.WPF.UI.EditorViaWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_editor_via_window.html", null ]
    ] ],
    [ "Computers.WPFClient.IMainLogic", "interface_computers_1_1_w_p_f_client_1_1_i_main_logic.html", [
      [ "Computers.WPFClient.MainLogic", "class_computers_1_1_w_p_f_client_1_1_main_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Computers.WPF.BL.IProcessorLogic", "interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_processor_logic.html", [
      [ "Computers.WPF.BL.ProcessorLogic", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html", null ]
    ] ],
    [ "Computers.Repository.IRepository< T >", "interface_computers_1_1_repository_1_1_i_repository.html", [
      [ "Computers.Repository.ComputersRepo< T >", "class_computers_1_1_repository_1_1_computers_repo.html", null ]
    ] ],
    [ "Computers.Repository.IRepository< Company >", "interface_computers_1_1_repository_1_1_i_repository.html", [
      [ "Computers.Repository.ICompanyRepository", "interface_computers_1_1_repository_1_1_i_company_repository.html", [
        [ "Computers.Repository.CompanyRepository", "class_computers_1_1_repository_1_1_company_repository.html", null ]
      ] ]
    ] ],
    [ "Computers.Repository.IRepository< Motherboard >", "interface_computers_1_1_repository_1_1_i_repository.html", [
      [ "Computers.Repository.IMotherboardRepository", "interface_computers_1_1_repository_1_1_i_motherboard_repository.html", [
        [ "Computers.Repository.MotherboardRepository", "class_computers_1_1_repository_1_1_motherboard_repository.html", null ]
      ] ]
    ] ],
    [ "Computers.Repository.IRepository< Processor >", "interface_computers_1_1_repository_1_1_i_repository.html", [
      [ "Computers.Repository.IProcessorRepository", "interface_computers_1_1_repository_1_1_i_processor_repository.html", [
        [ "Computers.Repository.ProcessorRepository", "class_computers_1_1_repository_1_1_processor_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "Computers.WPF.MyIoc", "class_computers_1_1_w_p_f_1_1_my_ioc.html", null ],
      [ "Computers.WPF.MyIoc", "class_computers_1_1_w_p_f_1_1_my_ioc.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "Computers.WPF.UI.CompanyToBrushConverter", "class_computers_1_1_w_p_f_1_1_u_i_1_1_company_to_brush_converter.html", null ],
      [ "Computers.WPF.UI.IntToCompanyIdConverter", "class_computers_1_1_w_p_f_1_1_u_i_1_1_int_to_company_id_converter.html", null ],
      [ "Computers.WPF.UI.IntToCompanyNameConverter", "class_computers_1_1_w_p_f_1_1_u_i_1_1_int_to_company_name_converter.html", null ]
    ] ],
    [ "Computers.Program.MainComputer", "class_computers_1_1_program_1_1_main_computer.html", null ],
    [ "Computers.Logic.Tests.ManufactureLogicTests", "class_computers_1_1_logic_1_1_tests_1_1_manufacture_logic_tests.html", null ],
    [ "Computers.Web.Models.MapperFactory", "class_computers_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "Computers.Data.Motherboard", "class_computers_1_1_data_1_1_motherboard.html", null ],
    [ "ObservableObject", null, [
      [ "Computers.Data.Processor", "class_computers_1_1_data_1_1_processor.html", null ],
      [ "Computers.WPFClient.ProcessorVM", "class_computers_1_1_w_p_f_client_1_1_processor_v_m.html", null ]
    ] ],
    [ "Computers.Web.Models.Processor", "class_computers_1_1_web_1_1_models_1_1_processor.html", null ],
    [ "Computers.Web.Models.ProcessorListViewModel", "class_computers_1_1_web_1_1_models_1_1_processor_list_view_model.html", null ],
    [ "Computers.Web.Program", "class_computers_1_1_web_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Processors_ProcessorsDetails", "class_asp_net_core_1_1_views___processors___processors_details.html", null ],
      [ "AspNetCore.Views_Processors_ProcessorsEdit", "class_asp_net_core_1_1_views___processors___processors_edit.html", null ],
      [ "AspNetCore.Views_Processors_ProcessorsIndex", "class_asp_net_core_1_1_views___processors___processors_index.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Computers.Web.Models.Processor >>", null, [
      [ "AspNetCore.Views_Processors_ProcessorsList", "class_asp_net_core_1_1_views___processors___processors_list.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "Computers.WPF.MyIoc", "class_computers_1_1_w_p_f_1_1_my_ioc.html", null ],
      [ "Computers.WPF.MyIoc", "class_computers_1_1_w_p_f_1_1_my_ioc.html", null ]
    ] ],
    [ "Computers.Web.Startup", "class_computers_1_1_web_1_1_startup.html", null ],
    [ "ViewModelBase", null, [
      [ "Computers.WPF.VM.EditorViewModel", "class_computers_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "Computers.WPF.VM.MainViewModel", "class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", null ],
      [ "Computers.WPFClient.MainVM", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Computers.WPF.UI.ComputerEditorWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window.html", null ],
      [ "Computers.WPF.UI.ComputerEditorWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window.html", null ],
      [ "Computers.WPF.UI.ComputerEditorWindow", "class_computers_1_1_w_p_f_1_1_u_i_1_1_computer_editor_window.html", null ],
      [ "Computers.WPFClient.EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", null ],
      [ "Computers.WPFClient.EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", null ],
      [ "Computers.WPFClient.EditorWindow", "class_computers_1_1_w_p_f_client_1_1_editor_window.html", null ],
      [ "Computers.WPFClient.MainLogic", "class_computers_1_1_w_p_f_client_1_1_main_logic.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ],
      [ "Computers.WPFClient.MainWindow", "class_computers_1_1_w_p_f_client_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Computers.WPF.MainWindow", "class_computers_1_1_w_p_f_1_1_main_window.html", null ]
    ] ]
];