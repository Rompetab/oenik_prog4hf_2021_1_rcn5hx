var namespace_computers_1_1_repository =
[
    [ "CompanyRepository", "class_computers_1_1_repository_1_1_company_repository.html", "class_computers_1_1_repository_1_1_company_repository" ],
    [ "ComputersRepo", "class_computers_1_1_repository_1_1_computers_repo.html", "class_computers_1_1_repository_1_1_computers_repo" ],
    [ "ICompanyRepository", "interface_computers_1_1_repository_1_1_i_company_repository.html", "interface_computers_1_1_repository_1_1_i_company_repository" ],
    [ "IMotherboardRepository", "interface_computers_1_1_repository_1_1_i_motherboard_repository.html", "interface_computers_1_1_repository_1_1_i_motherboard_repository" ],
    [ "IProcessorRepository", "interface_computers_1_1_repository_1_1_i_processor_repository.html", "interface_computers_1_1_repository_1_1_i_processor_repository" ],
    [ "IRepository", "interface_computers_1_1_repository_1_1_i_repository.html", "interface_computers_1_1_repository_1_1_i_repository" ],
    [ "MotherboardRepository", "class_computers_1_1_repository_1_1_motherboard_repository.html", "class_computers_1_1_repository_1_1_motherboard_repository" ],
    [ "ProcessorRepository", "class_computers_1_1_repository_1_1_processor_repository.html", "class_computers_1_1_repository_1_1_processor_repository" ]
];