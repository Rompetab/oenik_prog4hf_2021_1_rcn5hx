var class_computers_1_1_data_1_1_processor =
[
    [ "Copy", "class_computers_1_1_data_1_1_processor.html#a4d3da5490de5a6183d489bfb4eb8a9f3", null ],
    [ "Company", "class_computers_1_1_data_1_1_processor.html#a2287d1d5182277eaa961c8619fbcf78f", null ],
    [ "CompanyId", "class_computers_1_1_data_1_1_processor.html#a1c85e6c90682c110b2d2b6248f49e450", null ],
    [ "CoreCount", "class_computers_1_1_data_1_1_processor.html#a012730da6b3b80d31fae49bd7345dbb1", null ],
    [ "Damaged", "class_computers_1_1_data_1_1_processor.html#a8854745375e1b56ea01ed5e22e3b45b0", null ],
    [ "Ghz", "class_computers_1_1_data_1_1_processor.html#ab3a466e0437105d7d225fa33f071c370", null ],
    [ "MainData", "class_computers_1_1_data_1_1_processor.html#a68d9ef35f8467af75f5c4a440bb4d1e1", null ],
    [ "Name", "class_computers_1_1_data_1_1_processor.html#a5b105a14f31a27456c21621e008c91c6", null ],
    [ "Price", "class_computers_1_1_data_1_1_processor.html#a3524b7ef2c6ad5ff4199f8f662eb9d5a", null ],
    [ "ProcId", "class_computers_1_1_data_1_1_processor.html#a14fce30e7574aa156f0265145f4224cf", null ],
    [ "Socket", "class_computers_1_1_data_1_1_processor.html#aa03d358b3afda7dbdb456ca310bb47f1", null ]
];