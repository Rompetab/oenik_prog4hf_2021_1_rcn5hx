var dir_e214cbbe4746eac28ea46365a53af71e =
[
    [ "Computers.Data", "dir_313a5f09a02a16a099a5d3d58dc3ea07.html", "dir_313a5f09a02a16a099a5d3d58dc3ea07" ],
    [ "Computers.Logic", "dir_7b2b9c6d9715684a2483be687768c9fe.html", "dir_7b2b9c6d9715684a2483be687768c9fe" ],
    [ "Computers.Logic.Tests", "dir_d2a76485a1d0048365a78669536b539b.html", "dir_d2a76485a1d0048365a78669536b539b" ],
    [ "Computers.Repository", "dir_e9addf111b5bdd88c38219009a333383.html", "dir_e9addf111b5bdd88c38219009a333383" ],
    [ "Computers.Web", "dir_4dd7f6db0b48fa60820d0f4ddab2f6cd.html", "dir_4dd7f6db0b48fa60820d0f4ddab2f6cd" ],
    [ "Computers.WPF", "dir_34f073c98ab537e863927ad44230e597.html", "dir_34f073c98ab537e863927ad44230e597" ],
    [ "Computers.WPFClient", "dir_dbbafc82cc33651d1a8f0db6b1d2c213.html", "dir_dbbafc82cc33651d1a8f0db6b1d2c213" ],
    [ "FF_RCN5HX", "dir_bb0a953ec0f2b9a2dab23f238dd88836.html", "dir_bb0a953ec0f2b9a2dab23f238dd88836" ]
];