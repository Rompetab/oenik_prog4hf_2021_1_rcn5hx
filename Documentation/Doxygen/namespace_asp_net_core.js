var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Processors_ProcessorsDetails", "class_asp_net_core_1_1_views___processors___processors_details.html", "class_asp_net_core_1_1_views___processors___processors_details" ],
    [ "Views_Processors_ProcessorsEdit", "class_asp_net_core_1_1_views___processors___processors_edit.html", "class_asp_net_core_1_1_views___processors___processors_edit" ],
    [ "Views_Processors_ProcessorsIndex", "class_asp_net_core_1_1_views___processors___processors_index.html", "class_asp_net_core_1_1_views___processors___processors_index" ],
    [ "Views_Processors_ProcessorsList", "class_asp_net_core_1_1_views___processors___processors_list.html", "class_asp_net_core_1_1_views___processors___processors_list" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
];