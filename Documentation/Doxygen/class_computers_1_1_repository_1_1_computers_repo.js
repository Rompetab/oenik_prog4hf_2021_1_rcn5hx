var class_computers_1_1_repository_1_1_computers_repo =
[
    [ "ComputersRepo", "class_computers_1_1_repository_1_1_computers_repo.html#a7bbed250f4c76e34fe21cdf0577f682e", null ],
    [ "GetAll", "class_computers_1_1_repository_1_1_computers_repo.html#adee0066aa2b4bad10c004e98ed513ba8", null ],
    [ "GetOne", "class_computers_1_1_repository_1_1_computers_repo.html#a8cdd0817d8addf9df841bcc23909182d", null ],
    [ "Insert", "class_computers_1_1_repository_1_1_computers_repo.html#a493d0a8f74f6d1c3e58b64270de73182", null ],
    [ "Remove", "class_computers_1_1_repository_1_1_computers_repo.html#a7954c6023d6850afde29241e7057b476", null ],
    [ "ctx", "class_computers_1_1_repository_1_1_computers_repo.html#ab1dec408543874c65c68a779d5af2141", null ]
];