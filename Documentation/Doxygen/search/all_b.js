var searchData=
[
  ['onconfiguring_161',['OnConfiguring',['../class_computers_1_1_data_1_1_model_1_1_computer_context.html#ade157ea91c07fb424a223981d3bb36af',1,'Computers::Data::Model::ComputerContext']]],
  ['onmodelcreating_162',['OnModelCreating',['../class_computers_1_1_data_1_1_model_1_1_computer_context.html#aba6541e9b2280eb5087cec8f32de5cf2',1,'Computers::Data::Model::ComputerContext']]],
  ['operationresult_163',['OperationResult',['../class_computers_1_1_web_1_1_controllers_1_1_api_result.html#a8db04a00a8f554a94896af04b5172fee',1,'Computers::Web::Controllers::ApiResult']]],
  ['overclockprocessor_164',['OverclockProcessor',['../class_computers_1_1_logic_1_1_assemble_logic.html#ab5101237885d642a8935eee75b778a7b',1,'Computers.Logic.AssembleLogic.OverclockProcessor()'],['../interface_computers_1_1_logic_1_1_i_assemble_logic.html#aa0fbef21e2143a400ee9d0a324911aeb',1,'Computers.Logic.IAssembleLogic.OverclockProcessor()']]]
];
