var searchData=
[
  ['maindata_446',['MainData',['../class_computers_1_1_data_1_1_company.html#a83e3b4cb7d8c2681abf3f5216b2b898a',1,'Computers.Data.Company.MainData()'],['../class_computers_1_1_data_1_1_motherboard.html#a5767252ccf6281ff57590dfda185f531',1,'Computers.Data.Motherboard.MainData()'],['../class_computers_1_1_data_1_1_processor.html#a68d9ef35f8467af75f5c4a440bb4d1e1',1,'Computers.Data.Processor.MainData()']]],
  ['modcmd_447',['ModCmd',['../class_computers_1_1_w_p_f_client_1_1_main_v_m.html#ad8844f59da1b8e9237401369b757a9c4',1,'Computers::WPFClient::MainVM']]],
  ['motherboardid_448',['MotherboardId',['../class_computers_1_1_data_1_1_motherboard.html#a71e82c68ca6e48d6f245388b42500151',1,'Computers::Data::Motherboard']]],
  ['motherboards_449',['Motherboards',['../class_computers_1_1_data_1_1_company.html#a94a8aac596a8101b579857a5c712eb7c',1,'Computers.Data.Company.Motherboards()'],['../class_computers_1_1_data_1_1_model_1_1_computer_context.html#a065aa3a0e3eb5b11cab83a9fec6b32b6',1,'Computers.Data.Model.ComputerContext.Motherboards()']]]
];
