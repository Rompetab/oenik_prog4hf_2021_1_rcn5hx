var searchData=
[
  ['deleteprocessor_353',['DeleteProcessor',['../interface_computers_1_1_w_p_f_1_1_b_l_1_1_i_processor_logic.html#aaa8f8f6534dadab18e0080a2949bdc18',1,'Computers.WPF.BL.IProcessorLogic.DeleteProcessor()'],['../class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#a661a4e319629c362ec50943217a104b6',1,'Computers.WPF.BL.ProcessorLogic.DeleteProcessor()']]],
  ['deloneprocessor_354',['DelOneProcessor',['../class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html#aafc27af6f4e03179bf24515f4b95420d',1,'Computers::Web::Controllers::ProcessorApiController']]],
  ['details_355',['Details',['../class_computers_1_1_web_1_1_controllers_1_1_processors_controller.html#a5befb1bbdb5d9e97716c976d6572bc38',1,'Computers::Web::Controllers::ProcessorsController']]],
  ['dispose_356',['Dispose',['../class_computers_1_1_w_p_f_client_1_1_main_logic.html#a7e92ca2d98336e79fba167f3e672f581',1,'Computers.WPFClient.MainLogic.Dispose()'],['../class_computers_1_1_w_p_f_client_1_1_main_logic.html#a1ae9ab3c0847b8c5d41efcfd73cd5fe5',1,'Computers.WPFClient.MainLogic.Dispose(bool disposing)']]]
];
