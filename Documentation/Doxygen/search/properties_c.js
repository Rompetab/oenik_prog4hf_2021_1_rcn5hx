var searchData=
[
  ['selectedproc_459',['SelectedProc',['../class_computers_1_1_w_p_f_client_1_1_main_v_m.html#af30d1a3cfb6933a7ae98f903cb0e2196',1,'Computers::WPFClient::MainVM']]],
  ['selectedprocessor_460',['SelectedProcessor',['../class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#aad153c60d935e9f2c6e0a576e5cb602b',1,'Computers::WPF::VM::MainViewModel']]],
  ['showrequestid_461',['ShowRequestId',['../class_computers_1_1_web_1_1_models_1_1_error_view_model.html#accbde1e4aa373d0f1518c42fc14a3324',1,'Computers::Web::Models::ErrorViewModel']]],
  ['size_462',['Size',['../class_computers_1_1_data_1_1_motherboard.html#a843e904a8709bfc2f5e324fc74221433',1,'Computers.Data.Motherboard.Size()'],['../class_computers_1_1_logic_1_1_company_worker_count.html#a769923961fcbbb5153ead600e2ccbd6e',1,'Computers.Logic.CompanyWorkerCount.Size()']]],
  ['socket_463',['Socket',['../class_computers_1_1_data_1_1_motherboard.html#a3db17ccd40e2839ae9204068b996cc2c',1,'Computers.Data.Motherboard.Socket()'],['../class_computers_1_1_data_1_1_processor.html#aa03d358b3afda7dbdb456ca310bb47f1',1,'Computers.Data.Processor.Socket()'],['../class_computers_1_1_web_1_1_models_1_1_processor.html#a90233e6675b74d2147a068503962790f',1,'Computers.Web.Models.Processor.Socket()'],['../class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#ac95da433f6cd755cd6b2a59f8b465e3e',1,'Computers.WPFClient.ProcessorVM.Socket()']]],
  ['supportemail_464',['SupportEmail',['../class_computers_1_1_data_1_1_company.html#a95b77b217ac4f0c38f27a596482c8c68',1,'Computers::Data::Company']]]
];
