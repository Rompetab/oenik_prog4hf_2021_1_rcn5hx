var indexSectionsWithContent =
{
  0: "acdefghilmnoprstvwx",
  1: "acefghimpsv",
  2: "acx",
  3: "acdeghilmoprst",
  4: "c",
  5: "acdegilmnoprsw",
  6: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Pages"
};

