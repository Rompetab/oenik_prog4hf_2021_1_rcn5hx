var searchData=
[
  ['price_452',['Price',['../class_computers_1_1_data_1_1_motherboard.html#a18e52d6032ac2c48e5a844c32ffe9808',1,'Computers.Data.Motherboard.Price()'],['../class_computers_1_1_data_1_1_processor.html#a3524b7ef2c6ad5ff4199f8f662eb9d5a',1,'Computers.Data.Processor.Price()'],['../class_computers_1_1_web_1_1_models_1_1_processor.html#a1fcf2371d3cbe4d5425e82623155536c',1,'Computers.Web.Models.Processor.Price()'],['../class_computers_1_1_w_p_f_client_1_1_processor_v_m.html#abda23327b4aa844aa4de27893bb6847c',1,'Computers.WPFClient.ProcessorVM.Price()']]],
  ['processor_453',['Processor',['../class_computers_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html#a8bacb8840dbdb2f509d2a74600ce6c88',1,'Computers::WPF::VM::EditorViewModel']]],
  ['processors_454',['Processors',['../class_computers_1_1_data_1_1_company.html#a52087b027d1e17ba658173881fb53e2b',1,'Computers.Data.Company.Processors()'],['../class_computers_1_1_data_1_1_model_1_1_computer_context.html#a4a930817d93cbf820d8dd933cd6781ca',1,'Computers.Data.Model.ComputerContext.Processors()'],['../class_computers_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a05d9c9c4639d11d97d070ccf483aca56',1,'Computers.WPF.VM.MainViewModel.Processors()']]],
  ['procid_455',['ProcId',['../class_computers_1_1_data_1_1_processor.html#a14fce30e7574aa156f0265145f4224cf',1,'Computers::Data::Processor']]]
];
