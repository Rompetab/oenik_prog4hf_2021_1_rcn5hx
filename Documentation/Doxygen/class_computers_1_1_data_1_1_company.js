var class_computers_1_1_data_1_1_company =
[
    [ "Equals", "class_computers_1_1_data_1_1_company.html#a619621fd77aa731516b24ab4d6e760b1", null ],
    [ "GetHashCode", "class_computers_1_1_data_1_1_company.html#a0a2d6cd3c32c7ca93ac0ae1f05d7e23c", null ],
    [ "CompanyId", "class_computers_1_1_data_1_1_company.html#aae3dd900daf6f74d3bbcfb8ef083c2c9", null ],
    [ "Language", "class_computers_1_1_data_1_1_company.html#a442b99e011311e030f5bc7392781339e", null ],
    [ "Location", "class_computers_1_1_data_1_1_company.html#a5ed0715f18ad383bde33478b8eb0905e", null ],
    [ "MainData", "class_computers_1_1_data_1_1_company.html#a83e3b4cb7d8c2681abf3f5216b2b898a", null ],
    [ "Motherboards", "class_computers_1_1_data_1_1_company.html#a94a8aac596a8101b579857a5c712eb7c", null ],
    [ "Name", "class_computers_1_1_data_1_1_company.html#a80db5517376b3e03fe7a5cd7f9c6aea7", null ],
    [ "Processors", "class_computers_1_1_data_1_1_company.html#a52087b027d1e17ba658173881fb53e2b", null ],
    [ "SupportEmail", "class_computers_1_1_data_1_1_company.html#a95b77b217ac4f0c38f27a596482c8c68", null ],
    [ "WorkerCount", "class_computers_1_1_data_1_1_company.html#acddb5cf2416834c17607e0f8479a9e23", null ]
];