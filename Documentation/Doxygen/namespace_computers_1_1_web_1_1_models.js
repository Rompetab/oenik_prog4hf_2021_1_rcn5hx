var namespace_computers_1_1_web_1_1_models =
[
    [ "ErrorViewModel", "class_computers_1_1_web_1_1_models_1_1_error_view_model.html", "class_computers_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_computers_1_1_web_1_1_models_1_1_mapper_factory.html", "class_computers_1_1_web_1_1_models_1_1_mapper_factory" ],
    [ "Processor", "class_computers_1_1_web_1_1_models_1_1_processor.html", "class_computers_1_1_web_1_1_models_1_1_processor" ],
    [ "ProcessorListViewModel", "class_computers_1_1_web_1_1_models_1_1_processor_list_view_model.html", "class_computers_1_1_web_1_1_models_1_1_processor_list_view_model" ]
];