var class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests =
[
    [ "Setup", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html#a80a849cf487c21865a1dc1e4db0657e8", null ],
    [ "TestCompatibleProcessorCount", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html#a5691a02bfa6371854041628cc9c84fca", null ],
    [ "TestOverclockProcessor", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html#a6a1fb6776199f1be109a41229e46aa50", null ],
    [ "TestRepairMotherboard", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html#a538e80fb52c539d3d95ba703d32c1c5a", null ],
    [ "TestRepairProcessor", "class_computers_1_1_logic_1_1_tests_1_1_assemble_logic_tests.html#a33d9148b470c744b8fc2b8872cada7f8", null ]
];