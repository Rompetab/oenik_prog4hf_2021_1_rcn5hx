var class_computers_1_1_data_1_1_model_1_1_computer_context =
[
    [ "ComputerContext", "class_computers_1_1_data_1_1_model_1_1_computer_context.html#aad59f3c9c43dad8cdbfcf2cf9bb64c7d", null ],
    [ "OnConfiguring", "class_computers_1_1_data_1_1_model_1_1_computer_context.html#ade157ea91c07fb424a223981d3bb36af", null ],
    [ "OnModelCreating", "class_computers_1_1_data_1_1_model_1_1_computer_context.html#aba6541e9b2280eb5087cec8f32de5cf2", null ],
    [ "Companies", "class_computers_1_1_data_1_1_model_1_1_computer_context.html#a954852d87dbf3baeb0b4ce7c87a4c75b", null ],
    [ "Motherboards", "class_computers_1_1_data_1_1_model_1_1_computer_context.html#a065aa3a0e3eb5b11cab83a9fec6b32b6", null ],
    [ "Processors", "class_computers_1_1_data_1_1_model_1_1_computer_context.html#a4a930817d93cbf820d8dd933cd6781ca", null ]
];