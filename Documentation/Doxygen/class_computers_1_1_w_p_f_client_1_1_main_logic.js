var class_computers_1_1_w_p_f_client_1_1_main_logic =
[
    [ "ApiDelProcessor", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#a87f397bb21f15fadbc5911a42b2607db", null ],
    [ "ApiEditProcessor", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#abddc30773a0e1a2178860a300a874b33", null ],
    [ "ApiGetProcessors", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#ac277365ea46fe47d5300506f4557d071", null ],
    [ "Dispose", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#a7e92ca2d98336e79fba167f3e672f581", null ],
    [ "Dispose", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#a1ae9ab3c0847b8c5d41efcfd73cd5fe5", null ],
    [ "EditProcessor", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#ac1d117afd1e3630a77724559104afdce", null ],
    [ "InitializeComponent", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#ac86d2ec2329d85d22b7b7a68455c281f", null ],
    [ "SendMessage", "class_computers_1_1_w_p_f_client_1_1_main_logic.html#a76d6dfe99d64927a5e9dc81f326bf863", null ]
];