var class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic =
[
    [ "ProcessorLogic", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#aaab44e1e3a9901de29335dbcb41ab05b", null ],
    [ "AddProcessor", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#a0e207d51dd0cb3b157ba9d8821ac0669", null ],
    [ "DeleteProcessor", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#a661a4e319629c362ec50943217a104b6", null ],
    [ "EditProcessor", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#acb423ce67b9c708a4330e3c534743132", null ],
    [ "GetAllCompanies", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#aa73496a12d37631bc20736153602ada9", null ],
    [ "GetAllProcessor", "class_computers_1_1_w_p_f_1_1_b_l_1_1_processor_logic.html#aa1efbffb4f9dfeba5aadb60916a2e482", null ]
];