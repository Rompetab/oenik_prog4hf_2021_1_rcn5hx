var namespace_computers_1_1_web_1_1_controllers =
[
    [ "ApiResult", "class_computers_1_1_web_1_1_controllers_1_1_api_result.html", "class_computers_1_1_web_1_1_controllers_1_1_api_result" ],
    [ "HomeController", "class_computers_1_1_web_1_1_controllers_1_1_home_controller.html", "class_computers_1_1_web_1_1_controllers_1_1_home_controller" ],
    [ "ProcessorApiController", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller.html", "class_computers_1_1_web_1_1_controllers_1_1_processor_api_controller" ],
    [ "ProcessorsController", "class_computers_1_1_web_1_1_controllers_1_1_processors_controller.html", "class_computers_1_1_web_1_1_controllers_1_1_processors_controller" ]
];