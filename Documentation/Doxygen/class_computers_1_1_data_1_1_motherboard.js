var class_computers_1_1_data_1_1_motherboard =
[
    [ "Equals", "class_computers_1_1_data_1_1_motherboard.html#af160d6c8de1e7300a2df93babb91fb5b", null ],
    [ "GetHashCode", "class_computers_1_1_data_1_1_motherboard.html#abc699177fc841be7500007ba73535731", null ],
    [ "Company", "class_computers_1_1_data_1_1_motherboard.html#a76cbba614fa7d3c117363a4c5033acd4", null ],
    [ "CompanyId", "class_computers_1_1_data_1_1_motherboard.html#a5c76d76481652a6bda28648a0ebec211", null ],
    [ "Damaged", "class_computers_1_1_data_1_1_motherboard.html#ab93455376075c075d773d53021fd10ae", null ],
    [ "MainData", "class_computers_1_1_data_1_1_motherboard.html#a5767252ccf6281ff57590dfda185f531", null ],
    [ "MotherboardId", "class_computers_1_1_data_1_1_motherboard.html#a71e82c68ca6e48d6f245388b42500151", null ],
    [ "Name", "class_computers_1_1_data_1_1_motherboard.html#a6f82b4bf444c7b64ce810a83c27b01c3", null ],
    [ "Price", "class_computers_1_1_data_1_1_motherboard.html#a18e52d6032ac2c48e5a844c32ffe9808", null ],
    [ "RamSlots", "class_computers_1_1_data_1_1_motherboard.html#a39e091663ab32fb872cdd4bffabb0806", null ],
    [ "Size", "class_computers_1_1_data_1_1_motherboard.html#a843e904a8709bfc2f5e324fc74221433", null ],
    [ "Socket", "class_computers_1_1_data_1_1_motherboard.html#a3db17ccd40e2839ae9204068b996cc2c", null ]
];