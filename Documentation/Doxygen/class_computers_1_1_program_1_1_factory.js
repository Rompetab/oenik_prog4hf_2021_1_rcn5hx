var class_computers_1_1_program_1_1_factory =
[
    [ "CreateAssembeLogic", "class_computers_1_1_program_1_1_factory.html#a5c7c40c684b416bbadcca75312bbefd1", null ],
    [ "CreateCompanyRepository", "class_computers_1_1_program_1_1_factory.html#a190aec8b062610de3313d99ab87d3c13", null ],
    [ "CreateDbContext", "class_computers_1_1_program_1_1_factory.html#a2f284a2adf6dcd0f1f2de944310c0362", null ],
    [ "CreateManufactureLogic", "class_computers_1_1_program_1_1_factory.html#a52308fae2c2328384f14bb9fc0207c64", null ],
    [ "CreateMotherboardRepository", "class_computers_1_1_program_1_1_factory.html#a6d7c3177f8335d930816e38a55002775", null ],
    [ "CreateProcessorRepository", "class_computers_1_1_program_1_1_factory.html#a0282a2814d4a92143154df84d7fb6f8c", null ]
];