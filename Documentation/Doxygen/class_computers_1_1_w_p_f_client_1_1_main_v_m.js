var class_computers_1_1_w_p_f_client_1_1_main_v_m =
[
    [ "MainVM", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#a662f10f0fa465322d3299263148c6f2a", null ],
    [ "MainVM", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#ab5383441dbe3ff1009236d3354158ff7", null ],
    [ "AddCmd", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#ace2c92c7207d225ecb310f9129f002af", null ],
    [ "AllProc", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#ae083df87b0a48cf9be349960b129de73", null ],
    [ "DelCmd", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#a6f717c49206c97e2d1d6281c03df5cb1", null ],
    [ "EditorFunc", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#a958738bdd9e6c7d8cdfedb6f80e4b864", null ],
    [ "LoadCmd", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#a324afa840390d2e5e644cf382f9c30b7", null ],
    [ "ModCmd", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#ad8844f59da1b8e9237401369b757a9c4", null ],
    [ "SelectedProc", "class_computers_1_1_w_p_f_client_1_1_main_v_m.html#af30d1a3cfb6933a7ae98f903cb0e2196", null ]
];