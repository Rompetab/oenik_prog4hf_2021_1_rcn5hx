var class_computers_1_1_logic_1_1_manufacture_logic =
[
    [ "ManufactureLogic", "class_computers_1_1_logic_1_1_manufacture_logic.html#a13c45eaae061810a32b48b5dda63a76d", null ],
    [ "AddCompany", "class_computers_1_1_logic_1_1_manufacture_logic.html#a0d4f28dab174d3e13d5f6c5b9c99d1ee", null ],
    [ "AddMotherboard", "class_computers_1_1_logic_1_1_manufacture_logic.html#acfa67b6c4de35124e25172204932c896", null ],
    [ "AddProcessor", "class_computers_1_1_logic_1_1_manufacture_logic.html#a5600082bb6adb3929eac5755494aab21", null ],
    [ "ASyncAvarageProcessorCost", "class_computers_1_1_logic_1_1_manufacture_logic.html#a8e4c1611df15fffba11172532e82b827", null ],
    [ "ASyncCompanyWorkerCount", "class_computers_1_1_logic_1_1_manufacture_logic.html#a019e6ff494e814eb0e84728d504277f6", null ],
    [ "AvarageProcessorCost", "class_computers_1_1_logic_1_1_manufacture_logic.html#a13440cb3052b239229326ace92586890", null ],
    [ "ChangeCompanyLocation", "class_computers_1_1_logic_1_1_manufacture_logic.html#a8903348b45e933c0d30de36a3c61098f", null ],
    [ "ChangeProcessor", "class_computers_1_1_logic_1_1_manufacture_logic.html#a507be400b6358d5733d34e04329b3a35", null ],
    [ "ChangeProcessorPrice", "class_computers_1_1_logic_1_1_manufacture_logic.html#a66101a50235b5f9ca6139e3090f9e533", null ],
    [ "CompanyWorkerCount", "class_computers_1_1_logic_1_1_manufacture_logic.html#ab61aee9688702650b019bebb15aa8750", null ],
    [ "GetAllCompany", "class_computers_1_1_logic_1_1_manufacture_logic.html#a22eb476036ebc0b2522c53a664b697a6", null ],
    [ "GetAllMotherboard", "class_computers_1_1_logic_1_1_manufacture_logic.html#a90c9dab4fe497d4459e08378aa443145", null ],
    [ "GetAllProcessor", "class_computers_1_1_logic_1_1_manufacture_logic.html#af5dc889c10ad70ab3b37fec80f1fb1d7", null ],
    [ "GetOneCompany", "class_computers_1_1_logic_1_1_manufacture_logic.html#a7e00942d7bf880f6a6585a7bf21b69d4", null ],
    [ "GetOneMotherboard", "class_computers_1_1_logic_1_1_manufacture_logic.html#a21049fd069b2e1ffa5394caf83c70981", null ],
    [ "GetOneProcessor", "class_computers_1_1_logic_1_1_manufacture_logic.html#a02b127372daad313e684b55c9d43c329", null ],
    [ "RemoveCompany", "class_computers_1_1_logic_1_1_manufacture_logic.html#a49acc1657a9d9c085c9ef4817110ec35", null ],
    [ "RemoveMotherboard", "class_computers_1_1_logic_1_1_manufacture_logic.html#ab8ef80ebc4afd9e4a85c98d48f30afb4", null ],
    [ "RemoveProcessor", "class_computers_1_1_logic_1_1_manufacture_logic.html#aa3b773690b9295f821660814d3a7d159", null ]
];