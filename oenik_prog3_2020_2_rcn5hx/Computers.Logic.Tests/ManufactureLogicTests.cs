﻿// <copyright file="ManufactureLogicTests.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Computers.Data;
    using Computers.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// ManufactureLogic method tests located here.
    /// </summary>
    public class ManufactureLogicTests
    {
        private Mock<ICompanyRepository> mockedCompanyRepo;
        private Mock<IMotherboardRepository> mockedMotherboardRepo;
        private Mock<IProcessorRepository> mockedProcessorRepo;

        /// <summary>
        /// Sets up the required Mock Repo instances.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedCompanyRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.mockedMotherboardRepo = new Mock<IMotherboardRepository>(MockBehavior.Loose);
            this.mockedProcessorRepo = new Mock<IProcessorRepository>(MockBehavior.Loose);
        }

        /// <summary>
        /// Tests, if it returns all the companies from the table.
        /// </summary>
        [Test]
        public void TestGetAllCompanies()
        {
            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);

            List<Company> companies = new List<Company>()
            {
                new Company() { Name = "Test Company#1" },
                new Company() { Name = "Test Company#2" },
                new Company() { Name = "Test Company#3" },
                new Company() { Name = "Test Company#4" },
                new Company() { Name = "Test Company#5" },
            };

            List<Company> test = mlogic.GetAllCompany().ToList();

            this.mockedCompanyRepo.Setup(x => x.GetAll()).Returns(companies.AsQueryable());
            this.mockedCompanyRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests, if it adds a new motherboard to the table.
        /// </summary>
        [Test]
        public void TestAddaNewMotherboard()
        {
            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);
            Motherboard test = new Motherboard { Name = "Test Motherboard" };
            this.mockedMotherboardRepo.Setup(x => x.Insert(test));

            mlogic.AddMotherboard(test.Name, test.Socket, test.Price, test.Size, test.RamSlots, test.Damaged, test.CompanyId);

            this.mockedMotherboardRepo.Verify(x => x.Insert(test), Times.Once);
        }

        /// <summary>
        /// Tests, if it returns one processor from the table.
        /// </summary>
        [Test]
        public void TestGetOneProcessor()
        {
            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);
            Processor test = new Processor { Name = "Test Processor" };

            Processor exp = mlogic.GetOneProcessor(10);

            this.mockedProcessorRepo.Setup(x => x.GetOne(It.IsAny<int>())).Returns(test);
            this.mockedProcessorRepo.Verify(x => x.GetOne(10), Times.Once);
        }

        /// <summary>
        /// Tests, if it removes one motherboard from the table.
        /// </summary>
        [Test]
        public void TestRemoveMotherboard()
        {
            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);
            Motherboard test = new Motherboard { Name = "Test Motherboard" };

            this.mockedMotherboardRepo.Setup(x => x.Remove(test)).Returns(It.IsAny<bool>());
            mlogic.RemoveMotherboard(test);
            this.mockedMotherboardRepo.Verify(x => x.Remove(test), Times.Once);
        }

        /// <summary>
        /// Tests, if it changes a company's location.
        /// </summary>
        [Test]
        public void TestChangeCompanyLocation()
        {
            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);
            string loc = "Test location";
            int id = 1;

            this.mockedCompanyRepo.Setup(x => x.ChangeLocation(id, loc));
            mlogic.ChangeCompanyLocation(id, loc);
            this.mockedCompanyRepo.Verify(x => x.ChangeLocation(id, loc), Times.Once);
        }

        /// <summary>
        /// Tests, if the CompanyWorkerCount method works.
        /// </summary>
        [Test]
        public void TestCompanyWorkerCount()
        {
            List<Motherboard> mot = new List<Motherboard>
            {
                new Motherboard { Name = "TestMotherboard#1", Size = "ATX", CompanyId = 10, },
                new Motherboard { Name = "TestMotherboard#2", Size = "mATX", CompanyId = 100, },
                new Motherboard { Name = "TestMotherboard#3", Size = "eATX", CompanyId = 100, },
            };
            List<Company> com = new List<Company>
            {
                new Company { Name = "TestCompany#1", WorkerCount = 1000, CompanyId = 10 },
                new Company { Name = "TestCompany#2", WorkerCount = 2000, CompanyId = 100 },
            };

            List<CompanyWorkerCount> exp = new List<CompanyWorkerCount>()
            {
                new CompanyWorkerCount { Company = "TestCompany#1", Name = "TestMotherboard#1", Size = "ATX", Workers = 1000 },
                new CompanyWorkerCount { Company = "TestCompany#2", Name = "TestMotherboard#2", Size = "mATX", Workers = 2000 },
                new CompanyWorkerCount { Company = "TestCompany#2", Name = "TestMotherboard#3", Size = "eATX", Workers = 2000 },
            };

            this.mockedCompanyRepo.Setup(x => x.GetAll()).Returns(com.AsQueryable);
            this.mockedMotherboardRepo.Setup(x => x.GetAll()).Returns(mot.AsQueryable);

            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);

            Assert.That(mlogic.CompanyWorkerCount, Is.EqualTo(exp));

            this.mockedCompanyRepo.Verify(x => x.GetAll(), Times.Once);
            this.mockedMotherboardRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests, if the ProcessorCost method works.
        /// </summary>
        [Test]
        public void TestAvarageProcessorCost()
        {
            List<Processor> proc = new List<Processor>
            {
                new Processor { Name = "TestProcessor#1", Price = 1000, CompanyId = 10 },
                new Processor { Name = "TestProcessor#2", Price = 2000, CompanyId = 10 },
                new Processor { Name = "TestProcessor#3", Price = 3000, CompanyId = 100 },
                new Processor { Name = "TestProcessor#3", Price = 4000, CompanyId = 100 },
            };

            List<Company> com = new List<Company>
            {
                new Company { Name = "TestCompany#1", WorkerCount = 1000, CompanyId = 10 },
                new Company { Name = "TestCompany#2", WorkerCount = 2000, CompanyId = 100 },
            };

            List<AvarageProcessorCost> exp = new List<AvarageProcessorCost>()
            {
                new AvarageProcessorCost() { Name = "TestCompany#1", Avarage = Math.Round((double)(1000 + 2000) / 2) },
                new AvarageProcessorCost() { Name = "TestCompany#2", Avarage = Math.Round((double)(3000 + 4000) / 2) },
            };

            this.mockedCompanyRepo.Setup(x => x.GetAll()).Returns(com.AsQueryable);
            this.mockedProcessorRepo.Setup(x => x.GetAll()).Returns(proc.AsQueryable);

            ManufactureLogic mlogic = new ManufactureLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object, this.mockedCompanyRepo.Object);

            Assert.That(mlogic.AvarageProcessorCost(), Is.EqualTo(exp));

            this.mockedProcessorRepo.Verify(x => x.GetAll(), Times.Once);
            this.mockedCompanyRepo.Verify(x => x.GetAll(), Times.Once);
        }
    }
}
