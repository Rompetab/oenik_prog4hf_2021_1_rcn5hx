﻿// <copyright file="AssembleLogicTests.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Computers.Data;
    using Computers.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// AssembleLogic method tests located here.
    /// </summary>
    [TestFixture]
    public class AssembleLogicTests
    {
        private Mock<IMotherboardRepository> mockedMotherboardRepo;
        private Mock<IProcessorRepository> mockedProcessorRepo;

        /// <summary>
        /// Sets up the required Mock Repo instances.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedMotherboardRepo = new Mock<IMotherboardRepository>(MockBehavior.Loose);
            this.mockedProcessorRepo = new Mock<IProcessorRepository>(MockBehavior.Loose);
        }

        /// <summary>
        /// Tests, if the CompatibleProcessorCount method works.
        /// </summary>
        [Test]
        public void TestCompatibleProcessorCount()
        {
            List<Motherboard> mot = new List<Motherboard>
            {
                new Motherboard { Name = "TestMotherboard#1", Socket = "AM4" },
                new Motherboard { Name = "TestMotherboard#2", Socket = "LGA1151" },
            };
            List<Processor> proc = new List<Processor>
            {
                new Processor { Name = "TestProcessor#1", Socket = "AM4" },
                new Processor { Name = "TestProcessor#2", Socket = "LGA1151" },
                new Processor { Name = "TestProcessor#3", Socket = "AM4" },
            };

            List<CompatibleProcessorCount> exp = new List<CompatibleProcessorCount>()
            {
                new CompatibleProcessorCount() { Name = "TestMotherboard#1", Count = 2 },
                new CompatibleProcessorCount() { Name = "TestMotherboard#2", Count = 1 },
            };

            this.mockedMotherboardRepo.Setup(x => x.GetAll()).Returns(mot.AsQueryable);
            this.mockedProcessorRepo.Setup(x => x.GetAll()).Returns(proc.AsQueryable);

            AssembleLogic alogic = new AssembleLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object);

            Assert.That(alogic.CompatibleProcessorCount, Is.EqualTo(exp));

            this.mockedProcessorRepo.Verify(x => x.GetAll(), Times.Once);
            this.mockedMotherboardRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests, if the OverclockProcessor method works.
        /// </summary>
        [Test]
        public void TestOverclockProcessor()
        {
            Processor test = new Processor { ProcId = 100, Company = new Company { Name = "AMD" }, Name = "Ryzen 5 test" };
            this.mockedProcessorRepo.Setup(x => x.GetOne(test.ProcId)).Returns(test);
            AssembleLogic alogic = new AssembleLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object);
            Assert.That(alogic.OverclockProcessor(test.ProcId), Is.EqualTo(true));
            this.mockedProcessorRepo.Verify(x => x.GetOne(test.ProcId), Times.Once);
        }

        /// <summary>
        /// Tests, if the RepairMotherboard method works.
        /// </summary>
        [Test]
        public void TestRepairMotherboard()
        {
            Motherboard test = new Motherboard { MotherboardId = 100, Damaged = true };
            this.mockedMotherboardRepo.Setup(x => x.GetOne(test.MotherboardId)).Returns(test);
            AssembleLogic alogic = new AssembleLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object);
            Assert.That(alogic.RepairMotherboard(test.MotherboardId), Is.EqualTo(true));
            this.mockedMotherboardRepo.Verify(x => x.GetOne(test.MotherboardId), Times.Once);
        }

        /// <summary>
        /// Tests, if the RepairProcessor method works.
        /// </summary>
        [Test]
        public void TestRepairProcessor()
        {
            Processor test = new Processor { ProcId = 100, Damaged = false };
            this.mockedProcessorRepo.Setup(x => x.GetOne(test.ProcId)).Returns(test);
            AssembleLogic alogic = new AssembleLogic(this.mockedProcessorRepo.Object, this.mockedMotherboardRepo.Object);
            Assert.That(alogic.RepairProcessor(test.ProcId), Is.EqualTo(false));
            this.mockedProcessorRepo.Verify(x => x.GetOne(test.ProcId), Times.Once);
        }
    }
}
