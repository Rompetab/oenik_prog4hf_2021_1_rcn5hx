﻿// <copyright file="ProcessorListViewModel.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The view model of the processor list page.
    /// </summary>
    public class ProcessorListViewModel
    {
        /// <summary>
        /// Gets or Sets the list of the processors.
        /// </summary>
        public IList<Processor> ListOfProcessor { get; set; }

        /// <summary>
        /// Gets or Sets the edited processor.
        /// </summary>
        public Processor EditedProcessor { get; set; }
    }
}
