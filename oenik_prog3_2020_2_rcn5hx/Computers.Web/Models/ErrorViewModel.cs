// <copyright file="ErrorViewModel.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Models
{
    using System;

    /// <summary>
    /// An ErrorViewModel class.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or Sets the id of a request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the request id is null or not.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
