﻿// <copyright file="Processor.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    // Form Model / MVC ViewModel egyaránt lesz

    /// <summary>
    /// A WebModel version of a processor.
    /// </summary>
    public class Processor
    {
        /// <summary>
        /// Gets or Sets the id of a processor.
        /// </summary>
        [Display(Name = "Processor Id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets the socket of a processor.
        /// </summary>
        [Display(Name = "Processor Socket Name")]
        [Required]
        public string Socket { get; set; }

        /// <summary>
        /// Gets or Sets the name of a processor.
        /// </summary>
        [Display(Name = "Processor Model Name")]
        [Required]
        [StringLength(30, MinimumLength = 6)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the Name of a company which produced the processor.
        /// </summary>
        [Display(Name = "Processor Company Name")]
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or Sets the price of a processor.
        /// </summary>
        [Display(Name = "Processor Price")]
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or Sets the ghz of a processor.
        /// </summary>
        [Display(Name = "Processor Ghz")]
        [Required]
        public double Ghz { get; set; }

        /// <summary>
        /// Gets or Sets the core count of a processor.
        /// </summary>
        [Display(Name = "Processor Core Count")]
        [Required]
        public int CoreCount { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether the processor is damaged or not.
        /// </summary>
        [Display(Name = "Processor Damage")]
        [Required]
        public bool Damaged { get; set; }
    }
}
