﻿// <copyright file="MapperFactory.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// A MapperFactory class which creates conversion between two type of processors.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Maps between the Data.Processor and the Web.Models.Processor.
        /// </summary>
        /// <returns>a mapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(ctf =>
            {
               ctf.CreateMap<Data.Processor, Web.Models.Processor>().
                   ForMember(dest => dest.Id, map => map.MapFrom(src => src.ProcId)).
                   ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                   ForMember(dest => dest.Ghz, map => map.MapFrom(src => src.Ghz)).
                   ForMember(dest => dest.CompanyName, map => map.MapFrom(src => src.Company == null ? string.Empty : src.Company.Name)).
                   ForMember(dest => dest.Price, map => map.MapFrom(src => src.Price)).
                   ForMember(dest => dest.Socket, map => map.MapFrom(src => src.Socket)).
                   ForMember(dest => dest.Damaged, map => map.MapFrom(src => src.Damaged)).
                   ForMember(dest => dest.CoreCount, map => map.MapFrom(src => src.CoreCount));
            });

            return config.CreateMapper();
        }
    }
}
