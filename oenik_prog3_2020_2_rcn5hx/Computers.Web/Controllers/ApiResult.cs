﻿// <copyright file="ApiResult.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Helper class, for the bool results.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or Sets a value indicating whether the operation was successful, or not.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
