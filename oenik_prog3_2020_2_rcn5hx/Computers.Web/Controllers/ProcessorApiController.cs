﻿// <copyright file="ProcessorApiController.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Computers.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controls the API version of the program.
    /// </summary>
    public class ProcessorApiController : Controller
    {
        private IManufactureLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessorApiController"/> class.
        /// </summary>
        /// <param name="logic">instance of the Manufacturelogic class.</param>
        /// <param name="mapper">a mapper instance.</param>
        public ProcessorApiController(IManufactureLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all the processors.
        /// </summary>
        /// <returns>a list of Models.processor.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Processor> GetAll()
        {
            var processors = this.logic.GetAllProcessor();
            return this.mapper.Map<IList<Data.Processor>, List<Models.Processor>>(processors);
        }

        /// <summary>
        /// Deletes a processor.
        /// </summary>
        /// <param name="id">the id of the processor.</param>
        /// <returns>true if the process was a success.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneProcessor(int id)
        {
           return new ApiResult() { OperationResult = this.logic.RemoveProcessor(this.logic.GetOneProcessor(id)) };
        }

        /// <summary>
        /// Adds one processor to the list.
        /// </summary>
        /// <param name="proc">a processor to be added.</param>
        /// <returns>true if the process was a success.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneProcessor(Models.Processor proc)
        {
            bool success = false;
            if (proc != null)
            {
                try
                {
                    this.logic.AddProcessor(proc.Name, proc.Socket, proc.Price, proc.Ghz, proc.CoreCount, proc.Damaged, proc.CompanyName == "AMD" ? 1 : 2);
                    success = true;
                }
                catch (ArgumentException)
                {
                    success = false;
                }
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies a processor.
        /// </summary>
        /// <param name="proc">the processor need to be modified.</param>
        /// <returns>true if the process was a success.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneProcessor(Models.Processor proc)
        {
            if (proc != null)
            {
                return new ApiResult() { OperationResult = this.logic.ChangeProcessor(proc.Id, proc.Socket, proc.Damaged, proc.Ghz, proc.CoreCount, proc.Name, proc.CompanyName == "AMD" ? 1 : 2, proc.Price) };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}
