﻿// <copyright file="ProcessorsController.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Computers.Logic;
    using Computers.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controls the web requests, and posts.
    /// </summary>
    public class ProcessorsController : Controller
    {
        private IManufactureLogic logic;
        private IMapper mapper;
        private ProcessorListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessorsController"/> class.
        /// </summary>
        /// <param name="logic">instance of an IManufactureLogic.</param>
        /// <param name="mapper">instance of an IMapper.</param>
        public ProcessorsController(IManufactureLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new ProcessorListViewModel();
            this.vm.EditedProcessor = new Models.Processor();

            var processors = this.logic.GetAllProcessor();
            this.vm.ListOfProcessor = this.mapper.Map<IList<Data.Processor>, List<Models.Processor>>(processors);
        }

        /// <summary>
        /// Creates the Index page of the Processors.
        /// </summary>
        /// <returns>an IActionResult, which creates a view.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("ProcessorsIndex", this.vm);
        }

        /// <summary>
        /// Gets the Details page.
        /// </summary>
        /// <param name="id">The id of a selected processor.</param>
        /// <returns>a details page of the given processor.</returns>
        public IActionResult Details(int id)
        {
            return this.View("ProcessorsDetails", this.GetProcessorModel(id));
        }

        /// <summary>
        /// Removes a processor of the given ID.
        /// </summary>
        /// <param name="id">the selected processor's id.</param>
        /// <returns>back to the Index page, with a new TempData.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            if (this.logic.RemoveProcessor(this.logic.GetOneProcessor(id)))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Gets an edit page, of the processor.
        /// </summary>
        /// <param name="id">the selected processor's id.</param>
        /// <returns>the index page of the processors..</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedProcessor = this.GetProcessorModel(id);
            return this.View("ProcessorsIndex", this.vm);
        }

        /// <summary>
        ///  Decides, if you want to add a new processor, or change an existing one, if you can change it, changes it.
        /// </summary>
        /// <param name="proc">an instance of a processor.</param>
        /// <param name="editAction">this string decides if you want to add, or change.</param>
        /// <returns>Returns back to the Index page, with a new TempData.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Processor proc, string editAction)
        {
            if (this.ModelState.IsValid && proc != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.logic.AddProcessor(proc.Name, proc.Socket, proc.Price, proc.Ghz, proc.CoreCount, proc.Damaged, proc.CompanyName == "AMD" ? 1 : 2);
                    }
                    catch (ArgumentException e)
                    {
                        this.TempData["editResult"] = "AddProc FAIL: " + e.Message;
                    }
                }
                else
                {
                    if (!this.logic.ChangeProcessor(proc.Id, proc.Socket, proc.Damaged, proc.Ghz, proc.CoreCount, proc.Name, proc.CompanyName == "AMD" ? 1 : 2, proc.Price))
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.TempData["editAction"] = "Edit";
                this.vm.EditedProcessor = proc;
                return this.View("ProcessorsIndex", this.vm);
            }
        }

        private Models.Processor GetProcessorModel(int id)
        {
            Data.Processor oneProc = this.logic.GetOneProcessor(id);
            return this.mapper.Map<Data.Processor, Models.Processor>(oneProc);
        }
    }
}
