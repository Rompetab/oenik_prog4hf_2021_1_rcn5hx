// <copyright file="Program.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// This is where the fun begins.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Builds the program.
        /// </summary>
        /// <param name="args">stuff.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Starts the webBrowser.
        /// </summary>
        /// <param name="args">stuff.</param>
        /// <returns>an IHostBuilder.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
