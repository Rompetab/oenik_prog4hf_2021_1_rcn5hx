﻿// <copyright file="ComputerContext.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This class generates the tables and the connections between them.
    /// </summary>
    public class ComputerContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerContext"/> class.
        /// </summary>
        public ComputerContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or Sets- the table of the company.
        /// </summary>
        public virtual DbSet<Company> Companies { get; set; }

        /// <summary>
        /// Gets or Sets- the table of the processors.
        /// </summary>
        public virtual DbSet<Processor> Processors { get; set; }

        /// <summary>
        /// Gets or Sets- the table of the motherboards.
        /// </summary>
        public virtual DbSet<Motherboard> Motherboards { get; set; }

        /// <summary>
        /// This method configures the SQL database.
        /// </summary>
        /// <param name="optionsBuilder">optionsBuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\ComputerDatabase.mdf;Integrated Security=True");
            }
        }

        /// <summary>
        /// this method filles the tables with data, and generates the connections between them.
        /// </summary>
        /// <param name="modelBuilder">modelBuilder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Company c0 = new Company() { CompanyId = 1, Name = "AMD", Language = "English", Location = "USA", WorkerCount = 100000, SupportEmail = "AMD@support.com" };
            Company c1 = new Company() { CompanyId = 2, Name = "Intel", Language = "English", Location = "USA", WorkerCount = 240000, SupportEmail = "Intel@support.com" };
            Company c2 = new Company() { CompanyId = 3, Name = "ASUS", Language = "Mandarin", Location = "Taiwan", WorkerCount = 500000, SupportEmail = "ASUS@support.com" };
            Company c3 = new Company() { CompanyId = 4, Name = "Gigabyte", Language = "Mandarin", Location = "Taiwan", WorkerCount = 134000, SupportEmail = "Gigabyte@support.com" };
            Company c4 = new Company() { CompanyId = 5, Name = "ASRock", Language = "Mandarin", Location = "Taiwan", WorkerCount = 70000, SupportEmail = "ASRock@support.com" };
            Company c5 = new Company() { CompanyId = 6, Name = "MSI", Language = "Mandarin", Location = "Taiwan", WorkerCount = 40000, SupportEmail = "MSI@support.com" };

            Processor p0 = new Processor() { ProcId = 1, Name = "Ryzen 5 3600", Socket = "AM4", Price = 75000, Ghz = 3.6, Damaged = true, CoreCount = 6 };
            Processor p1 = new Processor() { ProcId = 2, Name = "Ryzen 7 3800", Socket = "AM4", Price = 106000, Ghz = 3.9, Damaged = false, CoreCount = 8 };
            Processor p2 = new Processor() { ProcId = 3, Name = "Ryzen 9 3900x", Socket = "AM4", Price = 148000, Ghz = 3.8, Damaged = true, CoreCount = 12 };
            Processor p3 = new Processor() { ProcId = 4, Name = "Ryzen 5 5600", Socket = "AM4", Price = 75000, Ghz = 3.6, Damaged = true, CoreCount = 6 };
            Processor p4 = new Processor() { ProcId = 5, Name = "i5 6600", Socket = "LGA1151", Price = 97000, Ghz = 3.3, Damaged = false, CoreCount = 4 };
            Processor p5 = new Processor() { ProcId = 6, Name = "i7 7700k", Socket = "LGA1151", Price = 140000, Ghz = 4.2, Damaged = false, CoreCount = 4 };
            Processor p6 = new Processor() { ProcId = 7, Name = "i9 9900k", Socket = "LGA1151", Price = 140000, Ghz = 3.6, Damaged = false, CoreCount = 8 };

            Motherboard m0 = new Motherboard() { MotherboardId = 1, Name = "ROG STRIX B450-F", Socket = "AM4", Price = 39000, Size = "ATX", Damaged = false, RamSlots = 4 };
            Motherboard m1 = new Motherboard() { MotherboardId = 2, Name = "ROG TUF B450-PLUS", Socket = "AM4", Price = 25000, Size = "ATX", Damaged = true, RamSlots = 4 };
            Motherboard m2 = new Motherboard() { MotherboardId = 3, Name = "B450 TOMAHAWK MAX", Socket = "AM4", Price = 37000, Size = "ATX", Damaged = false, RamSlots = 4 };
            Motherboard m3 = new Motherboard() { MotherboardId = 4, Name = "H110M-DGS", Socket = "LGA1151", Price = 13500, Size = "Micro ATX", Damaged = false, RamSlots = 2 };
            Motherboard m4 = new Motherboard() { MotherboardId = 5, Name = "Z390-A PRO", Socket = "LGA1151", Price = 36500, Size = "ATX", Damaged = true, RamSlots = 4 };
            Motherboard m5 = new Motherboard() { MotherboardId = 6, Name = "B450M", Socket = "AM4", Price = 21000, Size = "Micro ATX", Damaged = true, RamSlots = 2 };

            p0.CompanyId = c0.CompanyId;
            p1.CompanyId = c0.CompanyId;
            p2.CompanyId = c0.CompanyId;
            p3.CompanyId = c0.CompanyId;
            p4.CompanyId = c1.CompanyId;
            p5.CompanyId = c1.CompanyId;
            p6.CompanyId = c1.CompanyId;

            m0.CompanyId = c2.CompanyId;
            m1.CompanyId = c2.CompanyId;
            m2.CompanyId = c5.CompanyId;
            m3.CompanyId = c4.CompanyId;
            m4.CompanyId = c5.CompanyId;
            m5.CompanyId = c3.CompanyId;

            if (modelBuilder != null)
            {
                modelBuilder.Entity<Processor>(entity =>
                {
                    entity.HasOne(processor => processor.Company)
                        .WithMany(company => company.Processors)
                        .HasForeignKey(processor => processor.CompanyId)
                        .OnDelete(DeleteBehavior.ClientSetNull);
                });

                modelBuilder.Entity<Motherboard>(entity =>
                {
                    entity.HasOne(motherboard => motherboard.Company)
                        .WithMany(company => company.Motherboards)
                        .HasForeignKey(motherboard => motherboard.CompanyId)
                        .OnDelete(DeleteBehavior.ClientSetNull);
                });

                modelBuilder.Entity<Company>().HasData(c0, c1, c2, c3, c4, c5);
                modelBuilder.Entity<Processor>().HasData(p0, p1, p2, p3, p4, p5, p6);
                modelBuilder.Entity<Motherboard>().HasData(m0, m1, m2, m3, m4, m5);
            }
        }
    }
}
