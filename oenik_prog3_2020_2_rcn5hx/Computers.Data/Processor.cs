﻿// <copyright file="Processor.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The enitity of the processors.
    /// </summary>
    public class Processor : ObservableObject
    {
        private string name;
        private string socket;
        private int price;
        private double ghz;
        private int corecount;
        private bool damaged;
        private int companyId;
        private Company company;

        /// <summary>
        /// Gets or Sets- a unique key for every processor.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProcId { get; set; }

        /// <summary>
        /// Gets or Sets- the name of the processor.
        /// </summary>
        [Required]
        public string Name { get => this.name; set => this.Set(ref this.name, value); }

        /// <summary>
        /// Gets or Sets- the socket, the processor has.
        /// </summary>
        public string Socket { get => this.socket; set => this.Set(ref this.socket, value); }

        /// <summary>
        /// Gets or Sets- the price you can buy the processor.
        /// </summary>
        public int Price { get => this.price; set => this.Set(ref this.price, value); }

        /// <summary>
        /// Gets or Sets- the maximum Ghz the processor runs at.
        /// </summary>
        public double Ghz { get => this.ghz; set => this.Set(ref this.ghz, value); }

        /// <summary>
        /// Gets or Sets- the amount of cores the processor has.
        /// </summary>
        public int CoreCount { get => this.corecount; set => this.Set(ref this.corecount, value); }

        /// <summary>
        /// Gets or Sets a value indicating whether- if the processor is damaged or not.
        /// </summary>
        public bool Damaged { get => this.damaged; set => this.Set(ref this.damaged, value); }

        /// <summary>
        /// Gets or Sets- the foreign key of the processor which points to it's manufacturer.
        /// </summary>
        [ForeignKey(nameof(Company))]
        public int CompanyId { get => this.companyId; set => this.Set(ref this.companyId, value); }

        /// <summary>
        /// Gets- all the data that a processor has.
        /// </summary>
        [NotMapped]
        public string MainData => $"[{this.ProcId}] : {this.Name}  (Socket: {this.Socket}) (Ghz: {this.Ghz}) (Price: {this.Price}) (CoreCount: {this.CoreCount})";

        /// <summary>
        /// Gets or Sets- the company navigation property.
        /// </summary>
        [NotMapped]
        public virtual Company Company { get => this.company; set => this.Set(ref this.company, value); }

        /// <summary>
        /// Copies a given processor instance, to this.
        /// </summary>
        /// <param name="proc">The given processor instance.</param>
        public void Copy(Processor proc)
        {
            if (proc != null)
            {
                this.ProcId = proc.ProcId;
                this.Name = proc.Name;
                this.Socket = proc.Socket;
                this.Price = proc.Price;
                this.CoreCount = proc.CoreCount;
                this.Ghz = proc.Ghz;
                this.Damaged = proc.Damaged;
                this.CompanyId = proc.CompanyId;
            }
        }
    }
}
