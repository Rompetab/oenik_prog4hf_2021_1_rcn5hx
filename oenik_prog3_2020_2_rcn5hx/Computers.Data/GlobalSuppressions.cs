﻿// <copyright file="GlobalSuppressions.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<had to set it, for a unit test>", Scope = "member", Target = "~P:Computers.Data.Company.Processors")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<had to set it, for a unit test>", Scope = "member", Target = "~P:Computers.Data.Company.Motherboards")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]