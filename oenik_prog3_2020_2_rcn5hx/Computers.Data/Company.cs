﻿// <copyright file="Company.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// The enitity of the companies.
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Gets or Sets- a unique key for every company.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompanyId { get; set; }

        /// <summary>
        /// Gets or Sets- The name of the company.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets- The language that the majority of the workers speak.
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or Sets- The location of the company.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or Sets- The amount of workers at the company.
        /// </summary>
        public int WorkerCount { get; set; }

        /// <summary>
        /// Gets or Sets- the email to make contact with the company.
        /// </summary>
        public string SupportEmail { get; set; }

        /// <summary>
        /// Gets- all the data that a company has.
        /// </summary>
        [NotMapped]
        public string MainData => $"[{this.CompanyId}] : {this.Name}  (Language: {this.Language}) (Location: {this.Location} WorkerCount: {this.WorkerCount}) (Support Email: {this.SupportEmail})";

        /// <summary>
        /// Gets or Sets- the collection of the processor navigation property.
        /// </summary>
        public virtual ICollection<Processor> Processors { get; set; }

        /// <summary>
        /// Gets or Sets- the collection of the motherboard navigation property.
        /// </summary>
        public virtual ICollection<Motherboard> Motherboards { get; set; }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">the object we want to compare to.</param>
        /// <returns>true, if they are equal.</returns>
        public override bool Equals(object obj)
        {
            Company comp = obj as Company;
            if (comp != null)
            {
                return this.Name == comp.Name &&
                       this.Language == comp.Language &&
                       this.Location == comp.Location &&
                       this.WorkerCount == comp.WorkerCount &&
                       this.SupportEmail == comp.SupportEmail;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode method.
        /// </summary>
        /// <returns>a unique code.</returns>
        public override int GetHashCode()
        {
            return (this.Location.Length * 7) + 200 - this.Name.Length + (this.SupportEmail.Length * 2);
        }
    }
}
