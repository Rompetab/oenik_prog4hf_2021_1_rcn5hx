﻿// <copyright file="Motherboard.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// The enitity of the motherboards.
    /// </summary>
    public class Motherboard
    {
        /// <summary>
        /// Gets or Sets- a unique key for every company.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MotherboardId { get; set; }

        /// <summary>
        /// Gets or Sets- the name of the motherboard.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets- the socket, the motherboard has.
        /// </summary>
        public string Socket { get; set; }

        /// <summary>
        /// Gets or Sets- the price you can buy the motherboard.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or Sets- the standard size of the motherboard (mATX, ATX, eATX etc.).
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// Gets or Sets- the amount of RAM slots, the motherboard has.
        /// </summary>
        public int RamSlots { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether- if the motherboard is damaged or not.
        /// </summary>
        public bool Damaged { get; set; }

        /// <summary>
        /// Gets or Sets- the foreign key of the motherboard which points to it's manufacturer.
        /// </summary>
        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }

        /// <summary>
        /// Gets- all the data that a motherboard has.
        /// </summary>
        [NotMapped]
        public string MainData => $"[{this.MotherboardId}] : {this.Name}  (Socket: {this.Socket}) (Size: {this.Size}) (Price: {this.Price}) (RamSlots: {this.RamSlots})";

        /// <summary>
        /// Gets or Sets- the company navigation property.
        /// </summary>
        [NotMapped]
        public virtual Company Company { get; set; }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">the object we want to compare to.</param>
        /// <returns>true, if they are equal.</returns>
        public override bool Equals(object obj)
        {
            Motherboard mot = obj as Motherboard;
            if (mot != null)
            {
                return this.Name == mot.Name &&
                       this.Socket == mot.Socket &&
                       this.Price == mot.Price &&
                       this.RamSlots == mot.RamSlots &&
                       this.Damaged == mot.Damaged &&
                       this.CompanyId == mot.CompanyId;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode method.
        /// </summary>
        /// <returns>a unique code.</returns>
        public override int GetHashCode()
        {
            return (this.CompanyId * 7) + 200 - this.Name.Length + (this.Socket.Length * 2);
        }
    }
}
