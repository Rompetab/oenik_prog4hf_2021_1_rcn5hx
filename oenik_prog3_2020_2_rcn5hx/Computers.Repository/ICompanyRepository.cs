﻿// <copyright file="ICompanyRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data;

    /// <summary>
    /// An interface for the CompanyRepository class.
    /// </summary>
    public interface ICompanyRepository : IRepository<Company>
    {
        /// <summary>
        /// A method used for changing the location of the given company.
        /// </summary>
        /// <param name="id">company id.</param>
        /// <param name="newLocation">new location.</param>
        void ChangeLocation(int id, string newLocation);
    }
}
