﻿// <copyright file="ProcessorRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Computers.Data;
    using Computers.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository class for Processor entity.
    /// </summary>
    public class ProcessorRepository : ComputersRepo<Processor>, IProcessorRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessorRepository"/> class.
        /// </summary>
        /// <param name="ctx">ctx.</param>
        public ProcessorRepository(DbContext ctx)
                                                : base(ctx)
        {
        }

        /// <summary>
        /// This method is used to change the name of the Processor.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="newName">name.</param>
        public void ChangeName(int id, string newName)
        {
            var processor = this.GetOne(id);
            if (processor != null)
            {
                processor.Name = newName;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist");
            }
        }

        /// <summary>
        /// method definition which changes a processor by the given id.
        /// </summary>
        /// <param name="id">id of the processor.</param>
        /// <param name="socket">socket of the processor.</param>
        /// <param name="damaged">damage of the processor.</param>
        /// <param name="ghz">ghz of the processor.</param>
        /// <param name="coreCount">Core count of the processor.</param>
        /// <param name="name">Name of the processor.</param>
        /// <param name="companyId">Company ID of the processor.</param>
        /// <param name = "price" > the price of the processor.</param>
        /// <returns>true if it can be changed.</returns>
        public bool ChangeProcessor(int id, string socket, bool damaged, double ghz, int coreCount, string name, int companyId, int price)
        {
            var actualproc = this.GetOne(id);
            if (actualproc != null)
            {
                actualproc.ProcId = id;
                actualproc.Socket = socket;
                actualproc.Damaged = damaged;
                actualproc.CoreCount = coreCount;
                actualproc.Name = name;
                actualproc.CompanyId = companyId;
                actualproc.Ghz = ghz;
                actualproc.Price = price;
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method gives back a processor entity, given by the id.
        /// </summary>
        /// <param name="id">The processor id.</param>
        /// <returns>A Processor entity.</returns>
        public override Processor GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ProcId == id);
        }

        /// <summary>
        /// This method inserts one processor entity into the table.
        /// </summary>
        /// <param name="entity">A processor entity.</param>
        public override void Insert(Processor entity)
        {
            (this.ctx as ComputerContext).Processors.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// This method removes a processor entity.
        /// </summary>
        /// <param name="entity">a processor entity.</param>
        /// <returns>a true, if it can be removed(and has been by the method).</returns>
        public override bool Remove(Processor entity)
        {
            if (entity != null && this.GetOne(entity.ProcId) != null)
            {
                (this.ctx as ComputerContext).Processors.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
