﻿// <copyright file="MotherboardRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Computers.Data;
    using Computers.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository class for motherboard entity.
    /// </summary>
    public class MotherboardRepository : ComputersRepo<Motherboard>, IMotherboardRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MotherboardRepository"/> class.
        /// </summary>
        /// <param name="ctx">ctx.</param>
        public MotherboardRepository(DbContext ctx)
                                                  : base(ctx)
        {
        }

        /// <summary>
        /// This method is used to change the socket of the motherboard.
        /// </summary>
        /// <param name="id">motherboard id.</param>
        /// <param name="newSocket">the new socket.</param>
        public void ChangeSocket(int id, string newSocket)
        {
            var motherboard = this.GetOne(id);
            if (motherboard != null)
            {
                motherboard.Socket = newSocket;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist");
            }
        }

        /// <summary>
        /// This method gives back a motherboard entity, given by the id.
        /// </summary>
        /// <param name="id">The motherboard id.</param>
        /// <returns>A motherboard entity.</returns>
        public override Motherboard GetOne(int id)
        {
                return this.GetAll().SingleOrDefault(x => x.MotherboardId == id);
        }

        /// <summary>
        /// This method inserts one motherboard entity into the table.
        /// </summary>
        /// <param name="entity">A motherboard entity.</param>
        public override void Insert(Motherboard entity)
        {
            (this.ctx as ComputerContext).Motherboards.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// This method removes a motherboard entity.
        /// </summary>
        /// <param name="entity">a motherboard entity.</param>
        /// <returns>a true, if it can be removed(and has been by the method).</returns>
        public override bool Remove(Motherboard entity)
        {
            if (entity != null && this.GetOne(entity.MotherboardId) != null)
            {
                (this.ctx as ComputerContext).Motherboards.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
