﻿// <copyright file="IMotherboardRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data;

    /// <summary>
    /// An interface for the MotherboardRepository class.
    /// </summary>
    public interface IMotherboardRepository : IRepository<Motherboard>
    {
        /// <summary>
        /// A method used for changing the socket on the given motherboard.
        /// </summary>
        /// <param name="id">motherboard id.</param>
        /// <param name="newSocket">new motherboard socket.</param>
        void ChangeSocket(int id, string newSocket);
    }
}
