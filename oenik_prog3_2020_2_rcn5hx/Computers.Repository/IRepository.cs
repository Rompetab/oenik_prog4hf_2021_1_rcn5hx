﻿// <copyright file="IRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Computers.Data;

    /// <summary>
    /// Generic interface which will be implemented by the specified classes.
    /// </summary>
    /// <typeparam name="T">a generic T variable.</typeparam>
    public interface IRepository<T>
                                   where T : class
    {
        /// <summary>
        /// A generic method which will be implemented by the specified classes, gives back one entity.
        /// </summary>
        /// <param name="id">generic id.</param>
        /// <returns>Generic T variable.</returns>
        T GetOne(int id);

        /// <summary>
        /// A generic method which will be implemented by the specified classes, gives back all the entities from the given list.
        /// </summary>
        /// <returns>Generic T variable.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// A generic method which will be implemented by the specified classes, inserts one entity.
        /// </summary>
        /// <param name="entity">a generic entitiy.</param>
        void Insert(T entity);

        /// <summary>
        /// A generic method which will be implemented by the specified classes, removes one entity.
        /// </summary>
        /// <param name="entity">a generic entitiy.</param>
        /// <returns>true, if it can be removed (and has been by the method).</returns>
        bool Remove(T entity);
    }
}
