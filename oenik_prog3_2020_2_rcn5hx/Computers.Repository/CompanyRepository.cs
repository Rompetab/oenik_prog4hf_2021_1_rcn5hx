﻿// <copyright file="CompanyRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using Computers.Data;
    using Computers.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository class for company entity.
    /// </summary>
    public class CompanyRepository : ComputersRepo<Company>, ICompanyRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyRepository"/> class.
        /// </summary>
        /// <param name="ctx">ctx.</param>
        public CompanyRepository(DbContext ctx)
                                              : base(ctx)
        {
        }

        /// <summary>
        /// This method is used to change the location of the company.
        /// </summary>
        /// <param name="id">company id.</param>
        /// <param name="newLocation">the new location.</param>
        public void ChangeLocation(int id, string newLocation)
        {
            var company = this.GetOne(id);
            if (company != null)
            {
                company.Location = newLocation;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist");
            }
        }

        /// <summary>
        /// This method gives back a company entity, given by the id.
        /// </summary>
        /// <param name="id">The company id.</param>
        /// <returns>A company entity.</returns>
        public override Company GetOne(int id)
        {
                return this.GetAll().SingleOrDefault(x => x.CompanyId == id);
        }

        /// <summary>
        /// This method inserts one company entity into the table.
        /// </summary>
        /// <param name="entity">A company entity.</param>
        public override void Insert(Company entity)
        {
            (this.ctx as ComputerContext).Companies.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// This method removes a company entity.
        /// </summary>
        /// <param name="entity">a company entity.</param>
        /// <returns>a true, if it can be removed(and has been by the method).</returns>
        public override bool Remove(Company entity)
        {
            if (entity != null && this.GetOne(entity.CompanyId) != null)
            {
                (this.ctx as ComputerContext).Companies.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
