﻿// <copyright file="IProcessorRepository.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data;

    /// <summary>
    /// An interface for the ProcessorRepository class.
    /// </summary>
    public interface IProcessorRepository : IRepository<Processor>
    {
        /// <summary>
        /// A method used for changing the name of the given processor.
        /// </summary>
        /// <param name="id">processor id.</param>
        /// <param name="newName">new processor name.</param>
        void ChangeName(int id, string newName);

        /// <summary>
        /// method definition which changes a processor by the given id.
        /// </summary>
        /// <param name="id">id of the processor.</param>
        /// <param name="socket">socket of the processor.</param>
        /// <param name="damaged">damage of the processor.</param>
        /// <param name="ghz">ghz of the processor.</param>
        /// <param name="coreCount">Core count of the processor.</param>
        /// <param name="name">Name of the processor.</param>
        /// <param name="companyId">Company ID of the processor.</param>
        /// <param name = "price" > the price of the processor.</param>
        /// <returns>true if it can be changed.</returns>
        public bool ChangeProcessor(int id, string socket, bool damaged, double ghz, int coreCount, string name, int companyId, int price);
    }
}
