﻿// <copyright file="ComputersRepo{T}.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Computers.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Abstract class which implements the IRepository interface, and the table's elements are the descendants.
    /// </summary>
    /// <typeparam name="T">generic.</typeparam>
    public abstract class ComputersRepo<T> : IRepository<T>
                                                        where T : class
    {
        /// <summary>
        /// An instance of the DbContext.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputersRepo{T}"/> class.
        /// </summary>
        /// <param name="ctx">Instance of a ComputerContext.</param>
        public ComputersRepo(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Gets all the elements in the given parameter.
        /// </summary>
        /// <returns>an iQueryable list.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// A generic methid which will be used by the descendants gives back one entity.
        /// </summary>
        /// <param name="id">a generic id.</param>
        /// <returns>with one element from the given list.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// A generic method which will be used by the descendants, inserts one entity.
        /// </summary>
        /// <param name="entity">a generic entity.</param>
        public abstract void Insert(T entity);

        /// <summary>
        /// A generic method which will be used by the descendants, removes one entity.
        /// </summary>
        /// <param name="entity">a generic entitiy.</param>
        /// <returns>true, if it can be removed (and has been by the method).</returns>
        public abstract bool Remove(T entity);
    }
}