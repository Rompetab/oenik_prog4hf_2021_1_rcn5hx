﻿// <copyright file="GlobalSuppressions.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<had to be protected>", Scope = "member", Target = "~F:Computers.Repository.ComputersRepo`1.ctx")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<its not visible for every class>", Scope = "member", Target = "~F:Computers.Repository.ComputersRepo`1.ctx")]
[assembly: SuppressMessage("Design", "CA1012:Abstract types should not have public constructors", Justification = "Reviewed", Scope = "type", Target = "~T:Computers.Repository.ComputersRepo`1")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]