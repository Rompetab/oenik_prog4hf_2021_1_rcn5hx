﻿// <copyright file="MainComputer.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Computers.Data;
    using Computers.Data.Model;
    using Computers.Logic;
    using Computers.Program;
    using Computers.Repository;
    using ConsoleTools;

    /// <summary>
    /// This where the fun begins.
    /// </summary>
    public static class MainComputer
    {
        /// <summary>
        /// Contains the menu, and the instaces of the classes used in the program.
        /// </summary>
        public static void Main()
        {
            ComputerContext ctx = Factory.CreateDbContext();
            MotherboardRepository mrepo = Factory.CreateMotherboardRepository(ctx);
            CompanyRepository crepo = Factory.CreateCompanyRepository(ctx);
            ProcessorRepository prepo = Factory.CreateProcessorRepository(ctx);
            ManufactureLogic mlogic = Factory.CreateManufactureLogic(prepo, mrepo, crepo);
            AssembleLogic alogic = Factory.CreateAssembeLogic(prepo, mrepo);
            Menu(mlogic, alogic);
        }

        /// <summary>
        /// The menu of the program.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void Menu(ManufactureLogic mlogic, AssembleLogic alogic)
        {
            ConsoleMenu menu = new ConsoleMenu();
            try
            {
                menu.Add("* List All *", () => ListAll(mlogic))
                .Add("* Company operations *", () => new ConsoleMenu()
                .Add("* List all Companies *", () => ListCompanies(mlogic))
                .Add("* List one Company *", () => GetCompany(mlogic))
                .Add("* Add a new Company *", () => AddCompany(mlogic))
                .Add("* Remove a Company *", () => RemoveCompany(mlogic))
                .Add("* Change the location of the company *", () => ChangeCompanyLocation(mlogic))
                .Add("* Previous page *", x => x.CloseMenu()).Show())
                .Add("* Processor operations *", () => new ConsoleMenu()
                .Add("* List all Processors *", () => ListProcessors(mlogic))
                .Add("* List one Processor *", () => GetProcessor(mlogic))
                .Add("* Add a new Processor *", () => AddProcessor(mlogic))
                .Add("* Remove a Processor *", () => RemoveProcessor(mlogic))
                .Add("* Change the price of a processor *", () => ChangeProcessorPrice(mlogic))
                .Add("* Change the name of the processor *", () => ChangeProcessorName(alogic))
                .Add("* Previous page *", x => x.CloseMenu()).Show())
                .Add("* Motherboard operations *", () => new ConsoleMenu()
                .Add("* List all Motherboards *", () => ListMotherboards(mlogic))
                .Add("* List one Motherboard *", () => GetMotherboard(mlogic))
                .Add("* Add a new Motherboard *", () => AddMotherboard(mlogic))
                .Add("* Remove a Motherboard *", () => RemoveMotherboard(mlogic))
                .Add("* Change the socket of the motherboard *", () => ChangeMotherboardSocket(alogic))
                .Add("* Previous page *", x => x.CloseMenu()).Show())
                .Add("* Non-CRUD methods *", () => new ConsoleMenu()
                .Add("* Compatible Motherboards *", () => CompatibleProcessorCount(alogic))
                .Add("* Company Worker Count *", () => CompanyWorkerCount(mlogic))
                .Add("* Avarage Processor Price *", () => AvarageProcessorCost(mlogic))
                .Add("* Processor/Motherboard compatibility *", () => Compatible(alogic))
                .Add("* (ASync) Company Worker Count *", () => ASyncCompanyWorkerCount(mlogic))
                .Add("* (ASync) Avarage Processor Price *", () => ASyncAvarageProcessorCost(mlogic))
                .Add("* (ASync) Compatible Motherboards *", () => ASyncCompatibleProcessorCount(alogic))
                .Add("* Previous page *", x => x.CloseMenu()).Show());
                menu.Show();
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Press a button..");
                Console.ReadKey();
                Menu(mlogic, alogic);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Menu(mlogic, alogic);
            }
        }

        /// <summary>
        /// Lists all the elements from the whole database.
        /// </summary>
        /// <param name="mlogic">the logic class, which contains the needed classes.</param>
        public static void ListAll(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                List<Processor> list0 = mlogic.GetAllProcessor().ToList();
                List<Company> list1 = mlogic.GetAllCompany().ToList();
                List<Motherboard> list2 = mlogic.GetAllMotherboard().ToList();
                Console.WriteLine("----All Companies----");
                foreach (var item in list1)
                {
                    Console.WriteLine(item.MainData);
                }

                Console.WriteLine("\n----All Processors----");
                foreach (var item in list0)
                {
                    Console.WriteLine(item.MainData);
                }

                Console.WriteLine("\n----All Motherboards----");
                foreach (var item in list2)
                {
                    Console.WriteLine(item.MainData);
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// Lists all the elements from the processor table.
        /// </summary>
        /// <param name="mlogic">the logic class, which contains the needed class.</param>
        public static void ListProcessors(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                List<Processor> list = mlogic.GetAllProcessor().ToList();
                foreach (var item in list)
                {
                    Console.WriteLine(item.MainData);
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// Lists all the elements from the company table.
        /// </summary>
        /// <param name="mlogic">the logic class, which contains the needed class.</param>
        public static void ListCompanies(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                List<Company> list = mlogic.GetAllCompany().ToList();
                foreach (var item in list)
                {
                    Console.WriteLine(item.MainData);
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// Lists all the elements from the motherboard table.
        /// </summary>
        /// <param name="mlogic">the logic class, which contains the needed class.</param>
        public static void ListMotherboards(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                List<Motherboard> list = mlogic.GetAllMotherboard().ToList();

                foreach (var item in list)
                {
                    Console.WriteLine(item.MainData);
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// Menu method which adds a company to the database by the parameters within the method.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void AddCompany(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                Console.Write("\nNew company name: ");
                string name = Console.ReadLine();
                Console.Write("\nNew company's language: ");
                string language = Console.ReadLine();
                Console.Write("\nNew company location: ");
                string location = Console.ReadLine();
                Console.Write("\nThe amount of workers at the new company: ");
                int workerCount = int.Parse(Console.ReadLine());
                Console.Write("\nThe support email of the new company: ");
                string supportEmail = Console.ReadLine();

                mlogic.AddCompany(name, language, location, workerCount, supportEmail);
                Console.WriteLine("\nA new company has been added!");
                Console.ReadKey();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist");
            }
        }

        /// <summary>
        /// Menu method which adds a processor to the database by the parameters within the method.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void AddProcessor(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                Console.Write("\nNew processor name: ");
                string name = Console.ReadLine();
                Console.Write("\nNew processor socket: ");
                string socket = Console.ReadLine();
                Console.Write("\nNew processor price: ");
                int price = int.Parse(Console.ReadLine());
                Console.Write("\nThe maximum Ghz the new processor runs at: ");
                double ghz = double.Parse(Console.ReadLine());
                Console.Write("\nThe amount of cores, the processor has: ");
                int coreCount = int.Parse(Console.ReadLine());
                Console.Write("\n1 - if damaged, 0 - if not: ");
                bool damaged = false;
                int decision = int.Parse(Console.ReadLine());
                if (decision == 1)
                {
                    damaged = true;
                }

                Console.WriteLine("The company's id, which produces this processor: ");
                int companyId = int.Parse(Console.ReadLine());

                mlogic.AddProcessor(name, socket, price, ghz, coreCount, damaged, companyId);
                Console.WriteLine("\nA new processor has been added!");
                Console.ReadKey();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist");
            }
        }

        /// <summary>
        /// Menu method which adds a motherboard to the database by the parameters within the method.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void AddMotherboard(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                Console.Write("\nNew motherboard name: ");
                string name = Console.ReadLine();
                Console.Write("\nNew motherboard socket: ");
                string socket = Console.ReadLine();
                Console.Write("\nNew motherboard price: ");
                int price = int.Parse(Console.ReadLine());
                Console.Write("\nNew motherboard size: ");
                string size = Console.ReadLine();
                Console.Write("\nThe amount of Ram slots the motherboard has: ");
                int ramSlots = int.Parse(Console.ReadLine());
                Console.Write("\n1 - if damaged, 0 - if not: ");
                bool damaged = false;
                int decision = int.Parse(Console.ReadLine());
                if (decision == 1)
                {
                    damaged = true;
                }

                Console.WriteLine("The company's id, which produces this motherboard: ");
                int companyId = int.Parse(Console.ReadLine());
                mlogic.AddMotherboard(name, socket, price, size, ramSlots, damaged, companyId);
                Console.WriteLine("\nA new motherboard has been added!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Menu method which removes a company in the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void RemoveCompany(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id, of the company you want to remove: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneCompany(id) != null)
            {
                mlogic.RemoveCompany(mlogic.GetOneCompany(id));
                Console.WriteLine("The company has been removed!");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("The company cannot be removed!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Menu method which removes a processor in the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void RemoveProcessor(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id, of the processor you want to remove: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneProcessor(id) != null)
            {
                mlogic.RemoveProcessor(mlogic.GetOneProcessor(id));
                Console.WriteLine("The processor has been removed!");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("The processor cannot be removed!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Menu method which removes a motherboard in the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void RemoveMotherboard(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id, of the motherboard you want to remove: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneMotherboard(id) != null)
            {
                mlogic.RemoveMotherboard(mlogic.GetOneMotherboard(id));
                Console.WriteLine("The motherboard has been removed!");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("The motherboard cannot be removed!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Menu method which changes a company's location in the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void ChangeCompanyLocation(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id of the company, which you want to change it's location: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneCompany(id) != null)
            {
                Console.Write("\nType in the new location of the company: ");
                string newLocation = Console.ReadLine();

                mlogic.ChangeCompanyLocation(id, newLocation);
                Console.WriteLine("Company's location has changed!");
            }
            else
            {
                throw new InvalidOperationException("One of the objects does not exist, or it is not in the list");
            }
        }

        /// <summary>
        /// Menu method which changes a processor's price in the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void ChangeProcessorPrice(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id of the processor, which you want to change it's price: ");
            int id = int.Parse(Console.ReadLine());

            if (mlogic != null && mlogic.GetOneProcessor(id) != null)
            {
                Console.Write("\nType in the amount of money you want to set: ");
                int newPrice = int.Parse(Console.ReadLine());

                mlogic.ChangeProcessorPrice(id, newPrice);
                Console.WriteLine("Processor's price has changed!");
            }
            else
            {
                throw new InvalidOperationException("One of the objects does not exist, or it is not in the list");
            }
        }

        /// <summary>
        /// Menu method which changes a processor's name in the database by the id.
        /// </summary>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void ChangeProcessorName(AssembleLogic alogic)
        {
            Console.Write("\nType in the id of the processor, which you want to change it's name: ");
            int id = int.Parse(Console.ReadLine());

            if (alogic != null && alogic.GetOneProcessor(id) != null)
            {
                Console.Write("\nType in the new name of the processor: ");
                string newName = Console.ReadLine();
                alogic.ChangeProcessorName(id, newName);
                Console.WriteLine("Processor's name has changed!");
            }
            else
            {
                throw new InvalidOperationException("One of the objects does not exist, or it is not in the list");
            }
        }

        /// <summary>
        /// Menu method which changes a motherboard's socket in the database by the id.
        /// </summary>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void ChangeMotherboardSocket(AssembleLogic alogic)
        {
            Console.Write("\nType in the id of the motherboard, which you want to change it's socket: ");
            int id = int.Parse(Console.ReadLine());
            if (alogic != null && alogic.GetOneMotherboard(id) != null)
            {
                Console.Write("\nType in the new socket of the motherboard: ");
                string newSocket = Console.ReadLine();
                alogic.ChangeMotherboardSocket(id, newSocket);
                Console.WriteLine("Motherboard's socket has changed");
            }
            else
            {
                throw new InvalidOperationException("One of the objects does not exist, or it is not in the list");
            }
        }

        /// <summary>
        /// Menu method which gets a company from the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void GetCompany(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id of the company, which you want to get: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneCompany(id) != null)
            {
                Console.WriteLine("\n" + mlogic.GetOneCompany(id).MainData);
                Console.ReadKey();
            }
            else
            {
                throw new InvalidOperationException("There is no company in the database with this id.");
            }
        }

        /// <summary>
        /// Menu method which gets a processor from the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void GetProcessor(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id of the processor, which you want to get: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneProcessor(id) != null)
            {
                Console.WriteLine("\n" + mlogic.GetOneProcessor(id).MainData);
                Console.ReadKey();
            }
            else
            {
                throw new InvalidOperationException("There is no processor in the database with this id.");
            }
        }

        /// <summary>
        /// Menu method which gets a motherboard from the database by the id.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void GetMotherboard(ManufactureLogic mlogic)
        {
            Console.Write("\nType in the id of the motherboard, which you want to get: ");
            int id = int.Parse(Console.ReadLine());
            if (mlogic != null && mlogic.GetOneMotherboard(id) != null)
            {
                Console.WriteLine("\n" + mlogic.GetOneMotherboard(id).MainData);
                Console.ReadKey();
            }
            else
            {
                throw new InvalidOperationException("There is no motherboard in the database with this id.");
            }
        }

        /// <summary>
        /// Groups by the motherboard names, and counts how many processor is compatible with each one.
        /// </summary>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void CompatibleProcessorCount(AssembleLogic alogic)
        {
            if (alogic != null)
            {
                foreach (var item in alogic.CompatibleProcessorCount())
                {
                    Console.WriteLine("Alaplap neve: " + item.Name + "  --  " + "Kompatibilis processzorok hozzá: " + item.Count);
                }
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Displays every motherboard's company, and it's workers.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void CompanyWorkerCount(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                foreach (var item in mlogic.CompanyWorkerCount())
                {
                    Console.WriteLine(item.Company + "  --  " + item.Workers + "  --  " + item.Name);
                }
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Avarages the processor prices out by it's manufacturer.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void AvarageProcessorCost(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                foreach (var item in mlogic.AvarageProcessorCost())
                {
                    Console.WriteLine(item.Name + "  --  " + item.Avarage);
                }
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Menu method which makes sure if the given processor and motherboard are compatible.
        /// </summary>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void Compatible(AssembleLogic alogic)
        {
            if (alogic != null)
            {
                Console.Write("Write down the id, of the processor you want to compare: ");
                int processorId = int.Parse(Console.ReadLine());
                Console.Write("\nWrite down the id, of the motherboard you want to compare: ");
                int motherboardId = int.Parse(Console.ReadLine());

                if (alogic.Compatible(processorId, motherboardId))
                {
                    Console.WriteLine("\n\nKompatibilis a két termék!");
                }
                else
                {
                    Console.WriteLine("\n\nNem kompatibilis a két termék!");
                }

                Console.ReadKey();
            }
        }

        /// <summary>
        /// Calls the async version of the CompatibleProcessorCount method.
        /// </summary>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void ASyncCompatibleProcessorCount(AssembleLogic alogic)
        {
            if (alogic != null)
            {
                foreach (var item in alogic.ASyncCompatibleProcessorCount().Result)
                {
                    Console.WriteLine("Alaplap neve: " + item.Name + "  --  " + "Kompatibilis processzorok hozzá: " + item.Count);
                }
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Calls the async version of the CompanyWorkerCount method.
        /// </summary>
        /// <param name="mlogic">Manufacture logic instance.</param>
        public static void ASyncCompanyWorkerCount(ManufactureLogic mlogic)
        {
            if (mlogic != null)
            {
                foreach (var item in mlogic.ASyncCompanyWorkerCount().Result)
                {
                    Console.WriteLine(item.Company + "  --  " + item.Workers + "  --  " + item.Name);
                }
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Calls the async version of the AvarageProcessorCount method.
        /// </summary>
        /// <param name="alogic">Assemble logic instance.</param>
        public static void ASyncAvarageProcessorCost(ManufactureLogic alogic)
        {
            if (alogic != null)
            {
                foreach (var item in alogic.ASyncAvarageProcessorCost().Result)
                {
                    Console.WriteLine(item.Name + "  --  " + item.Avarage);
                }
            }

            Console.ReadKey();
        }
    }
}
