﻿// <copyright file="Factory.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Program
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data.Model;
    using Computers.Logic;
    using Computers.Repository;

    /// <summary>
    /// Instanciates new classes.
    /// </summary>
    public static class Factory
    {
        /// <summary>
        /// Creates an instance of the ComputerContext class.
        /// </summary>
        /// <returns>a ComputerContext instance.</returns>
        public static ComputerContext CreateDbContext()
        {
            return new ComputerContext();
        }

        /// <summary>
        /// Creates an instance of the CompanyRepository class.
        /// </summary>
        /// <param name="ctx">A ComputerContext instance.</param>
        /// <returns>a CompanyRepository instance.</returns>
        public static CompanyRepository CreateCompanyRepository(ComputerContext ctx)
        {
            return new CompanyRepository(ctx);
        }

        /// <summary>
        /// Creates an instance of the MotherboardRepository class.
        /// </summary>
        /// <param name="ctx">A ComputerContext instance.</param>
        /// <returns>a MotherboardRepository instance.</returns>
        public static MotherboardRepository CreateMotherboardRepository(ComputerContext ctx)
        {
            return new MotherboardRepository(ctx);
        }

        /// <summary>
        /// Creates an instance of the ProcessorRepository class.
        /// </summary>
        /// <param name="ctx">A ComputerContext instance.</param>
        /// <returns>a ProcessorRepository instance.</returns>
        public static ProcessorRepository CreateProcessorRepository(ComputerContext ctx)
        {
            return new ProcessorRepository(ctx);
        }

        /// <summary>
        /// Creates an instance of the ManufactureLogic class.
        /// </summary>
        /// <param name="proc">a Repository of the processor.</param>
        /// <param name="mot">a Repository of the motherboard.</param>
        /// <param name="com">a Repository of the company.</param>
        /// <returns>a ManufactureLogic instance.</returns>
        public static ManufactureLogic CreateManufactureLogic(ProcessorRepository proc, MotherboardRepository mot, CompanyRepository com)
        {
            return new ManufactureLogic(proc, mot, com);
        }

        /// <summary>
        /// Creates an instance of the AssembleLogic class.
        /// </summary>
        /// <param name="proc">a Repository of the processor.</param>
        /// <param name="mot">a Repository of the motherboard.</param>
        /// <returns>an AssembleLogic instance.</returns>
        public static AssembleLogic CreateAssembeLogic(ProcessorRepository proc, MotherboardRepository mot)
        {
            return new AssembleLogic(proc, mot);
        }
    }
}
