﻿// <copyright file="MainViewModel.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using Computers.Data;
    using Computers.WPF.BL;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// ViewModel class used on MainWindow.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IProcessorLogic procLogic;
        private Processor selectedProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="procLogic">a logic which contains all the necessary methods.</param>
        public MainViewModel(IProcessorLogic procLogic)
        {
            this.Processors = new ObservableCollection<Processor>();
            this.procLogic = procLogic;
            this.selectedProcessor = new Processor();

            this.AddCommand = new RelayCommand(() => procLogic.AddProcessor(this.Processors), true);
            this.EditCommand = new RelayCommand(() => procLogic.EditProcessor(this.SelectedProcessor), true);
            this.RemoveCommand = new RelayCommand(() => procLogic.DeleteProcessor(this.selectedProcessor, this.Processors), true);

            if (this.IsInDesignMode)
            {
                this.Processors.Add(new Processor { Name = "Ryzen 5600x", CoreCount = 12, Ghz = 3.2, CompanyId = 1, Socket = "AM4" });
            }
            else
            {
                this.procLogic.GetAllProcessor().ToList().ForEach(x => this.Processors.Add(x));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IProcessorLogic>())
        {
        }

        /// <summary>
        /// Gets a list of processors.
        /// </summary>
        public ObservableCollection<Processor> Processors { get; private set; }

        /// <summary>
        /// Gets or Sets a selected processor from the ListBox.
        /// </summary>
        public Processor SelectedProcessor { get => this.selectedProcessor; set => this.Set(ref this.selectedProcessor, value); }

        /// <summary>
        /// Gets a command which opens a new edit window and a new processor can be added.
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets a command which opens a new edit window and modify a processor.
        /// </summary>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets a command which can be used to delete a processor.
        /// </summary>
        public ICommand RemoveCommand { get; private set; }
    }
}
