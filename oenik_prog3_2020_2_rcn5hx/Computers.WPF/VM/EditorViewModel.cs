﻿// <copyright file="EditorViewModel.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This will be used for the editor window.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private Processor processor;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.processor = new Processor();
            if (this.IsInDesignMode)
            {
                this.processor.Name = "Ryzen 5600";
                this.processor.Damaged = true;
            }
        }

        /// <summary>
        /// Gets enum range of 0 to 2.
        /// </summary>
        public static IList<int> Companies
        {
            get
            {
                return Enumerable.Range(0, 2).ToList();
            }
        }

        /// <summary>
        /// Gets or Sets a processor.
        /// </summary>
        public Processor Processor { get => this.processor; set => this.Set(ref this.processor, value); }
    }
}
