﻿// <copyright file="IntToCompanyIdConverter.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Creates a list of all the company's names, and numbers.
    /// </summary>
    public class IntToCompanyIdConverter : IValueConverter
    {
        /// <summary>
        /// gives back a list with the processor companies.
        /// </summary>
        /// <param name="value">The data which will be converted.</param>
        /// <param name="targetType">the type of the target.</param>
        /// <param name="parameter">the parameter we need.</param>
        /// <param name="culture">info.</param>
        /// <returns>a color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IList<string> list = new List<string>();
            list.Add("AMD");
            list.Add("Intel");
            return list;
        }

        /// <summary>
        /// gives back a list from 1, to 2.
        /// </summary>
        /// <param name="value">The data which will be converted.</param>
        /// <param name="targetType">the type of the target.</param>
        /// <param name="parameter">the parameter we need.</param>
        /// <param name="culture">info.</param>
        /// <returns>a color.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Enumerable.Range(0, 2).ToList();
        }
    }
}
