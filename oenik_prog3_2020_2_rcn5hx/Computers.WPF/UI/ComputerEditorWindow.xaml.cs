﻿// <copyright file="ComputerEditorWindow.xaml.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Computers.Data;
    using Computers.WPF.VM;

    /// <summary>
    /// Interaction logic for ComputerEditorWindow.xaml.
    /// </summary>
    public partial class ComputerEditorWindow : Window
    {
        private EditorViewModel editorvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerEditorWindow"/> class.
        /// </summary>
        public ComputerEditorWindow()
        {
            this.InitializeComponent();
            this.editorvm = this.FindResource("EditorViewModel") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerEditorWindow"/> class.
        /// </summary>
        /// <param name="proc">the processor needs to be added or edited.</param>
        public ComputerEditorWindow(Processor proc)
            : this()
        {
            this.editorvm.Processor = proc;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
