﻿// <copyright file="CompanyToBrushConverter.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Coverts color, from a company name information.
    /// </summary>
    public class CompanyToBrushConverter : IValueConverter
    {
        /// <summary>
        /// gives back a color given upon the company's name.
        /// </summary>
        /// <param name="value">The data which will be converted.</param>
        /// <param name="targetType">the type of the target.</param>
        /// <param name="parameter">the parameter we need.</param>
        /// <param name="culture">info.</param>
        /// <returns>a color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int com = (int)value;

            if (com == 1)
            {
                return Brushes.LightCoral;
            }

            if (com == 2)
            {
                return Brushes.LightBlue;
            }

            return Brushes.LightGreen;
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="value">The data which will be converted.</param>
        /// <param name="targetType">the type of the target.</param>
        /// <param name="parameter">.</param>
        /// <param name="culture">..</param>
        /// <returns>nothing.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
