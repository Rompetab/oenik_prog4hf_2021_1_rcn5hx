﻿// <copyright file="IntToCompanyNameConverter.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converts a number into a company's name, and vice versa.
    /// </summary>
    public class IntToCompanyNameConverter : IValueConverter
    {
        /// <summary>
        /// gives back a company name, given upon a number.
        /// </summary>
        /// <param name="value">The data which will be converted.</param>
        /// <param name="targetType">the type of the target.</param>
        /// <param name="parameter">the parameter we need.</param>
        /// <param name="culture">info.</param>
        /// <returns>a company name.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int number = (int)value;
            switch (number)
            {
                default:
                case 1: return "AMD";
                case 2: return "Intel";
            }
        }

        /// <summary>
        /// gives back a number, given upon a company's name.
        /// </summary>
        /// <param name="value">The data which will be converted.</param>
        /// <param name="targetType">the type of the target.</param>
        /// <param name="parameter">the parameter we need.</param>
        /// <param name="culture">info.</param>
        /// <returns>a number.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string company = (string)value;

            switch (company)
            {
                default:
                case "AMD": return 1;
                case "Intel": return 2;
            }
        }
    }
}
