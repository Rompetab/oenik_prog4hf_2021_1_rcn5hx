﻿// <copyright file="EditorViaWindow.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;
    using Computers.WPF.BL;

    /// <summary>
    /// the class which implements the IEditorService interface.
    /// </summary>
    public class EditorViaWindow : IEditorService
    {
        /// <summary>
        /// A method used for editing a processor.
        /// </summary>
        /// <param name="proc">the processor which can be edited.</param>
        /// <returns>true if the user hit OK.</returns>
        public bool EditProcessor(Processor proc)
        {
            ComputerEditorWindow window = new ComputerEditorWindow(proc);
            return window.ShowDialog() ?? false;
        }
    }
}
