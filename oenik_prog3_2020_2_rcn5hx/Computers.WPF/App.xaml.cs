﻿// <copyright file="App.xaml.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using Computers.Data.Model;
    using Computers.Logic;
    using Computers.Repository;
    using Computers.WPF.BL;
    using Computers.WPF.UI;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<ICompanyRepository, CompanyRepository>();
            MyIoc.Instance.Register<IProcessorRepository, ProcessorRepository>();
            MyIoc.Instance.Register<IMotherboardRepository, MotherboardRepository>();

            MyIoc.Instance.Register<IManufactureLogic, ManufactureLogic>();
            MyIoc.Instance.Register<IProcessorLogic, ProcessorLogic>();

            MyIoc.Instance.Register<IEditorService, EditorViaWindow>();

            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);

            MyIoc.Instance.Register<DbContext, ComputerContext>();
        }
    }
}
