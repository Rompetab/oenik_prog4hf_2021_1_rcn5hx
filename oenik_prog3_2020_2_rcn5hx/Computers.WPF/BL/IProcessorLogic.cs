﻿// <copyright file="IProcessorLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;

    /// <summary>
    /// An interface for the ProcessorLogic class.
    /// </summary>
    public interface IProcessorLogic
    {
        /// <summary>
        /// A method used for adding a new processor to the database.
        /// </summary>
        /// <param name="proclist">the list which the UI uses.</param>
        void AddProcessor(IList<Processor> proclist);

        /// <summary>
        /// A method used for editing a processor.
        /// </summary>
        /// <param name="proc">the processor which can be edited.</param>
        void EditProcessor(Processor proc);

        /// <summary>
        /// A method used for deleting a processor.
        /// </summary>
        /// <param name="proc">the processor which needs to be deleted.</param>
        /// <param name="proclist">the list which the UI uses.</param>
        void DeleteProcessor(Processor proc, IList<Processor> proclist);

        /// <summary>
        /// A method used for getting a list of all the processors.
        /// </summary>
        /// <returns>a list of processors.</returns>
        IList<Processor> GetAllProcessor();
    }
}
