﻿// <copyright file="IEditorService.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;

    /// <summary>
    /// Interface used for editing a processor.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edits a processor.
        /// </summary>
        /// <param name="proc">a given processor.</param>
        /// <returns>if the action was succesful or not.</returns>
        bool EditProcessor(Processor proc);
    }
}
