﻿// <copyright file="ProcessorLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;
    using Computers.Logic;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// All the CRUD methods implemented.
    /// </summary>
    public class ProcessorLogic : IProcessorLogic
    {
        private readonly IEditorService editorService;
        private readonly IMessenger messengerService;
        private readonly IManufactureLogic mlogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessorLogic"/> class.
        /// </summary>
        /// <param name="editorservice">instance of a class which implements the IEditorService interface.</param>
        /// <param name="messengerService">this makes sure, the user gets a message according to their actions.</param>
        /// <param name="mlogic">This interface has all the CRUD methods, and the relationship between all the tables are declared here.</param>
        public ProcessorLogic(IEditorService editorservice, IMessenger messengerService, IManufactureLogic mlogic)
        {
            this.editorService = editorservice;
            this.messengerService = messengerService;
            this.mlogic = mlogic;
        }

        /// <summary>
        /// Gets back all the companies.
        /// </summary>
        /// <returns>a list of all the companies.</returns>
        public IList<Company> GetAllCompanies()
        {
            return this.mlogic.GetAllCompany();
        }

        /// <summary>
        /// A method used for adding a new processor to the database.
        /// </summary>
        /// <param name="proclist">the list which the UI uses.</param>
        public void AddProcessor(IList<Processor> proclist)
        {
            Processor proc = new Processor() { CompanyId = 1 };

            if (this.editorService.EditProcessor(proc))
            {
                this.mlogic.AddProcessor(proc.Name, proc.Socket, proc.Price, proc.Ghz, proc.CoreCount, proc.Damaged, proc.CompanyId);
                if (proclist != null)
                {
                    proclist.Add(proc);
                }

                this.messengerService.Send("Add successful", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Add failed", "LogicResult");
            }
        }

        /// <summary>
        /// A method used for deleting a processor.
        /// </summary>
        /// <param name="proc">the processor which needs to be deleted.</param>
        /// <param name="proclist">the list which the UI uses.</param>
        public void DeleteProcessor(Processor proc, IList<Processor> proclist)
        {
            if (proclist != null)
            {
                if (proc != null && proclist.Remove(proc))
                {
                    this.mlogic.RemoveProcessor(proc);
                    this.messengerService.Send("Remove successful", "LogicResult");
                }
                else
                {
                    this.messengerService.Send("Remove failed", "LogicResult");
                }
            }
        }

        /// <summary>
        /// A method used for editing a processor.
        /// </summary>
        /// <param name="proc">the processor which can be edited.</param>
        public void EditProcessor(Processor proc)
        {
            if (proc == null)
            {
                this.messengerService.Send("Edit failed", "LogicResult");
                return;
            }

            Processor cloneSave = new Processor();
            cloneSave.Copy(proc);
            if (this.editorService.EditProcessor(cloneSave))
            {
                proc.Copy(cloneSave);
                this.messengerService.Send("Edit successful", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Edit failed", "LogicResult");
            }
        }

        /// <summary>
        /// A method used for getting a list of all the processors.
        /// </summary>
        /// <returns>a list of processors.</returns>
        public IList<Processor> GetAllProcessor()
        {
            return this.mlogic.GetAllProcessor();
        }
    }
}
