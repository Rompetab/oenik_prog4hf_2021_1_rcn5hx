﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Dispose interface was inherited.", Scope = "member", Target = "~M:Computers.WPFClient.MainLogic.ApiEditProcessor(Computers.WPFClient.ProcessorVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It cannot be, because the program sets the list, every time.", Scope = "member", Target = "~P:Computers.WPFClient.MainVM.AllProc")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]