﻿// <copyright file="MainVM.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPFClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// The Main View Model for the API .
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private ProcessorVM selectedProc;
        private ObservableCollection<ProcessorVM> allProc;

        /// <summary>
        /// Initializes a new instance of the <see cref=" MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref=" MainVM"/> class.
        /// </summary>
        /// <param name="logic">a logic instance.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;
            this.LoadCmd = new RelayCommand(() => this.AllProc = new ObservableCollection<ProcessorVM>(this.logic.ApiGetProcessors()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelProcessor(this.selectedProc));
            this.AddCmd = new RelayCommand(() => this.logic.EditProcessor(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditProcessor(this.selectedProc, this.EditorFunc));
        }

        /// <summary>
        /// Gets or Sets the selected processor in the list.
        /// </summary>
        public ProcessorVM SelectedProc { get => this.selectedProc; set => this.Set(ref this.selectedProc, value); }

        /// <summary>
        /// Gets or Sets the list of the processors in the database.
        /// </summary>
        public ObservableCollection<ProcessorVM> AllProc { get => this.allProc; set => this.Set(ref this.allProc, value); }

        /// <summary>
        /// Gets or Sets how the editing should be done.
        /// </summary>
        public Func<ProcessorVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets a command for adding a processor.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets a command for deleting a processor.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets a command for modifying a processor.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets a command for loading a processor.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
