﻿// <copyright file="MainLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPFClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Manages the CRUD methods.
    /// </summary>
    public class MainLogic : IMainLogic, IDisposable
    {
        private string url = "https://localhost:44348/ProcessorApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Gets the processor list from the controller class.
        /// </summary>
        /// <returns>a processor list.</returns>
        public IList<ProcessorVM> ApiGetProcessors()
        {
            string json = this.client.GetStringAsync(new Uri(this.url + "all")).Result;
            var list = JsonSerializer.Deserialize<List<ProcessorVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// Deletes a processor.
        /// </summary>
        /// <param name="proc">processor instance.</param>
        public void ApiDelProcessor(ProcessorVM proc)
        {
            bool success = false;
            if (proc != null)
            {
                string json = this.client.GetStringAsync(new Uri(this.url + "del/" + Convert.ToString(proc.Id, CultureInfo.CurrentCulture))).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Sets the postdata for the processor, whether it need to be edited, or not.
        /// </summary>
        /// <param name="proc">processor instance.</param>
        /// <param name="isEditing">tells if it needs to be edited, or added.</param>
        /// <returns>true, if it can be added or edited.</returns>
        public bool ApiEditProcessor(ProcessorVM proc, bool isEditing)
        {
            if (proc != null)
            {
                string myUrl = this.url + (isEditing ? "mod" : "add");

                Dictionary<string, string> postData = new Dictionary<string, string>();
                if (isEditing)
                {
                    postData.Add("id", proc.Id.ToString(CultureInfo.CurrentCulture));
                }

                postData.Add("name", proc.Name);
                postData.Add("companyName", proc.CompanyName);
                postData.Add("price", proc.Price.ToString(CultureInfo.CurrentCulture));
                postData.Add("damaged", proc.Damaged.ToString());
                postData.Add("socket", proc.Socket);
                postData.Add("ghz", proc.Ghz.ToString(CultureInfo.CurrentCulture));
                postData.Add("coreCount", proc.CoreCount.ToString(CultureInfo.CurrentCulture));
                string json = this.client.PostAsync(new Uri(myUrl), new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                JsonDocument doc = JsonDocument.Parse(json);
                return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            return false;
        }

        /// <summary>
        /// Edits or adds a processor.
        /// </summary>
        /// <param name="proc">processor needs to be added or modified.</param>
        /// <param name="editorFunc">Func whitch tells how to do the opeation.</param>
        public void EditProcessor(ProcessorVM proc, Func<ProcessorVM, bool> editorFunc)
        {
            ProcessorVM clone = new ProcessorVM();
            if (proc != null)
            {
                clone.CopyFrom(proc);
            }

            bool? success = editorFunc?.Invoke(clone);

            if (success == true)
            {
                if (proc != null)
                {
                    success = this.ApiEditProcessor(clone, true);
                }
                else
                {
                    success = this.ApiEditProcessor(clone, false);
                }
            }

            this.SendMessage(success == true);
        }

        /// <summary>
        /// Sends back a message, indicating if the operation was successful or not.
        /// </summary>
        /// <param name="success">indicating if the operation was successful or not.</param>
        public void SendMessage(bool success)
        {
                string msg = success ? "Operation completed successfully!" : "Operation failed!";
                Messenger.Default.Send(msg, "ProcResult");
        }

        /// <summary>
        /// Disposes Httpclient.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes Httpclient.
        /// </summary>
        /// <param name="disposing">Bool indicating whether the disposing process is happening.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.client.Dispose();
            }
        }
    }
}
