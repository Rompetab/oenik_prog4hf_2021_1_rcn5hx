﻿// <copyright file="IMainLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPFClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An interface for the logic of the API program.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Sends back a message, indicating if the operation was successful or not.
        /// </summary>
        /// <param name="success">indicating if the operation was successful or not.</param>
        void SendMessage(bool success);

        /// <summary>
        /// Gets all the processors in the database using the API controller.
        /// </summary>
        /// <returns>a list of processors.</returns>
        IList<ProcessorVM> ApiGetProcessors();

        /// <summary>
        /// Deletes a processor.
        /// </summary>
        /// <param name="proc">a processor instance.</param>
        void ApiDelProcessor(ProcessorVM proc);

        /// <summary>
        /// Helps the serialization, based on the state of the editing.
        /// </summary>
        /// <param name="proc">the processor which needs to be modified.</param>
        /// <param name="isEditing">a bool indicates if its in a editing state, or not.</param>
        /// <returns>true, if it was editied.</returns>
        bool ApiEditProcessor(ProcessorVM proc, bool isEditing);

        /// <summary>
        /// Edits a processor.
        /// </summary>
        /// <param name="proc">the processor which needs to be modified.</param>
        /// <param name="editorFunc">a bool indicates if its in a editing state, or not.</param>
        void EditProcessor(ProcessorVM proc, Func<ProcessorVM, bool> editorFunc);
    }
}
