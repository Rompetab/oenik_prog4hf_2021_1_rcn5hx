﻿// <copyright file="ProcessorVM.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.WPFClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Contains all the information about a processor.
    /// </summary>
    public class ProcessorVM : ObservableObject
    {
        private int id;
        private string socket;
        private string name;
        private string companyName;
        private int price;
        private double ghz;
        private int coreCount;
        private bool damaged;

        /// <summary>
        /// Gets or Sets the id of a processor.
        /// </summary>
        public int Id { get => this.id; set => this.Set(ref this.id, value); }

        /// <summary>
        /// Gets or Sets the socket of a processor.
        /// </summary>
        public string Socket { get => this.socket; set => this.Set(ref this.socket, value); }

        /// <summary>
        /// Gets or Sets the name of a processor.
        /// </summary>
        public string Name { get => this.name; set => this.Set(ref this.name, value); }

        /// <summary>
        /// Gets or Sets the Name of a company which produced the processor.
        /// </summary>
        public string CompanyName { get => this.companyName; set => this.Set(ref this.companyName, value); }

        /// <summary>
        /// Gets or Sets the price of a processor.
        /// </summary>
        public int Price { get => this.price; set => this.Set(ref this.price, value); }

        /// <summary>
        /// Gets or Sets the ghz of a processor.
        /// </summary>
        public double Ghz { get => this.ghz; set => this.Set(ref this.ghz, value); }

        /// <summary>
        /// Gets or Sets the core count of a processor.
        /// </summary>
        public int CoreCount { get => this.coreCount; set => this.Set(ref this.coreCount, value); }

        /// <summary>
        /// Gets or Sets a value indicating whether the processor is damaged or not.
        /// </summary>
        public bool Damaged { get => this.damaged; set => this.Set(ref this.damaged, value); }

        /// <summary>
        /// Copies an instance's data to this data.
        /// </summary>
        /// <param name="other">an other instance of the processorVM.</param>
        public void CopyFrom(ProcessorVM other)
        {
            if (other != null)
            {
                this.id = other.id;
                this.companyName = other.companyName;
                this.coreCount = other.coreCount;
                this.ghz = other.ghz;
                this.name = other.name;
                this.price = other.price;
                this.socket = other.socket;
                this.damaged = other.damaged;
            }

            return;
        }
    }
}
