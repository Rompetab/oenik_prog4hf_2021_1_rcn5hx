﻿// <copyright file="GlobalSuppressions.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "<had to use ToUpper, so it wont make a difference between the capital and normal characters.>", Scope = "member", Target = "~M:Computers.Logic.AssembleLogic.OverclockProcessor(System.Int32)~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<had to use contains>", Scope = "member", Target = "~M:Computers.Logic.AssembleLogic.OverclockProcessor(System.Int32)~System.Boolean")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]