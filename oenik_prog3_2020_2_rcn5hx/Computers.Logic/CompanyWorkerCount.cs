﻿// <copyright file="CompanyWorkerCount.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Contains all the property, that the CompanyWorkerCount method needs.
    /// </summary>
    public class CompanyWorkerCount
    {
        /// <summary>
        /// Gets or Sets the name of the company.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Gets or Sets the name of the motherboard.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the size of the motherboard.
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// Gets or Sets the amount of workers, the company has.
        /// </summary>
        public int Workers { get; set; }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">the object we want to compare to.</param>
        /// <returns>true, if they are equal.</returns>
        public override bool Equals(object obj)
        {
            CompanyWorkerCount cwc = obj as CompanyWorkerCount;

            if (cwc != null)
            {
                return this.Company == cwc.Company &&
                   this.Name == cwc.Name &&
                   this.Size == cwc.Size &&
                   this.Workers == cwc.Workers;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode method.
        /// </summary>
        /// <returns>a unique code.</returns>
        public override int GetHashCode()
        {
            return (this.Company.Length * 6) + (45 * this.Name.Length) - (10 * this.Size.Length);
        }
    }
}
