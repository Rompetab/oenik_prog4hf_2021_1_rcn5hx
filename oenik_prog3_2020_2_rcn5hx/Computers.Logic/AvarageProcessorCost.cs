﻿// <copyright file="AvarageProcessorCost.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Contains all the property, that the AvarageProcessorCost method needs.
    /// </summary>
    public class AvarageProcessorCost
    {
        /// <summary>
        /// Gets or Sets the name of the company.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the avarage price of the processors, by the company.
        /// </summary>
        public double Avarage { get; set; }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">the object we want to compare to.</param>
        /// <returns>true, if they are equal.</returns>
        public override bool Equals(object obj)
        {
            AvarageProcessorCost apc = obj as AvarageProcessorCost;
            if (apc != null)
            {
                return this.Name == apc.Name &&
                       this.Avarage == apc.Avarage;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode method.
        /// </summary>
        /// <returns>a unique code.</returns>
        public override int GetHashCode()
        {
            return (this.Name.Length * 8) + 350;
        }
    }
}
