﻿// <copyright file="IManufactureLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data;

    /// <summary>
    /// A interface which describes the reliationship between motherboard, processor and the company.
    /// </summary>
    public interface IManufactureLogic : IComputersLogic
    {
        /// <summary>
        /// method definition which returns one of the companies by the given id.
        /// </summary>
        /// <returns>Company entity.</returns>
        /// <param name="id">an id of the company.</param>
        Company GetOneCompany(int id);

        /// <summary>
        /// method definition which returns a list with all the companies in the database.
        /// </summary>
        /// <returns>Processor IList.</returns>
        IList<Company> GetAllCompany();

        /// <summary>
        /// method definition which adds a motherboard to the database by the given parameters.
        /// </summary>
        /// <param name="name">The name of the new motherboard.</param>
        /// <param name="socket">The socket which the motherboard has.</param>
        /// <param name="price">The price you have to pay for this motherboard.</param>
        /// <param name="size">The size of the motherboard (mATX, ATX, eATX etc.).</param>
        /// <param name="ramSlots">The amount of RAM slots the motherboard has.</param>
        /// <param name="damaged">A value indicating whether- if the processor is damaged or not.</param>
        /// <param name="companyId">The id of the company which produses this new motherboard.</param>
        void AddMotherboard(string name, string socket, int price, string size, int ramSlots, bool damaged, int companyId);

        /// <summary>
        /// method definition which adds a processor to the database by the given parameters.
        /// </summary>
        /// <param name="name">The name of the new processor.</param>
        /// <param name="socket">The socket of the new processor.</param>
        /// <param name="price">The price you have to pay for this processor.</param>
        /// <param name="ghz">The maximum Ghz the new processor runs at.</param>
        /// <param name="coreCount">The amount of cores the new processor has.</param>
        /// <param name="damaged">A value indicating whether- if the processor is damaged or not.</param>
        /// <param name="companyId">The id of the company which produses this new processor.</param>
        void AddProcessor(string name, string socket, int price, double ghz, int coreCount, bool damaged, int companyId);

        /// <summary>
        /// method definition which adds a company to the database by the given parameters.
        /// </summary>
        /// <param name="name">The name of the new company.</param>
        /// <param name="language">The language of the new company.</param>
        /// <param name="location">The location where the company is.</param>
        /// <param name="workerCount">The amount of workers in the new company.</param>
        /// <param name="supportEmail">The email which you can contract the new company.</param>
        void AddCompany(string name, string language, string location, int workerCount, string supportEmail);

        /// <summary>
        /// method definition which removes a motherboard from the database by the given id.
        /// </summary>
        /// <param name="entity">an entity of a motherboard.</param>
        void RemoveMotherboard(Motherboard entity);

        /// <summary>
        /// method definition which removes a processor from the database by the given id.
        /// </summary>
        /// <param name="entity">an entity of a processor.</param>
        /// <returns>true if it can be removed.</returns>
        bool RemoveProcessor(Processor entity);

        /// <summary>
        /// method definition which removes a company from the database by the given id.
        /// </summary>
        /// <param name="entity">an entity of a company.</param>
        void RemoveCompany(Company entity);

        /// <summary>
        /// method definition which changes the price of the processor by the given id.
        /// </summary>
        /// <param name="id">a processor's id.</param>
        /// <param name="newPrice">the new price which will be set.</param>
        void ChangeProcessorPrice(int id, int newPrice);

        /// <summary>
        /// method definition which changes the location of the company by the given id.
        /// </summary>
        /// <param name="id">a company's id.</param>
        /// <param name="newLocation">the new lotacion which will be set.</param>
        void ChangeCompanyLocation(int id, string newLocation);

        /// <summary>
        /// method definition which changes a processor by the given id.
        /// </summary>
        /// <param name="id">id of the processor.</param>
        /// <param name="socket">socket of the processor.</param>
        /// <param name="damaged">damage of the processor.</param>
        /// <param name="ghz">ghz of the processor.</param>
        /// <param name="coreCount">Core count of the processor.</param>
        /// <param name="name">Name of the processor.</param>
        /// <param name="companyId">Company ID of the processor.</param>
        /// <param name = "price" > the price of the processor.</param>
        /// <returns>true if it can be changed.</returns>
        bool ChangeProcessor(int id, string socket, bool damaged, double ghz, int coreCount, string name, int companyId, int price);
    }
}
