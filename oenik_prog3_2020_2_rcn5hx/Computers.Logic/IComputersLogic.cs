﻿// <copyright file="IComputersLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data;

    /// <summary>
    /// a base interface which contains the must-have method definitions for both of the interfaces which implements this.
    /// </summary>
    public interface IComputersLogic
    {
        /// <summary>
        /// method definition which returns a list with all the motherboards in the database.
        /// </summary>
        /// <returns>Motherboard IList.</returns>
        IList<Motherboard> GetAllMotherboard();

        /// <summary>
        /// method definition which returns a list with all the processors in the database.
        /// </summary>
        /// <returns>Processor IList.</returns>
        IList<Processor> GetAllProcessor();

        /// <summary>
        /// method definition which returns one of the processors by the given id.
        /// </summary>
        /// <returns>Processor entity.</returns>
        /// <param name="id">an id of the processor.</param>
        Processor GetOneProcessor(int id);

        /// <summary>
        /// method definition which returns one of the motherboards by the given id.
        /// </summary>
        /// <returns>Motherboard entity.</returns>
        /// <param name="id">an id of the motherboard.</param>
        Motherboard GetOneMotherboard(int id);
    }
}
