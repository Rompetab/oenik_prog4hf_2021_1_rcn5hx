﻿// <copyright file="CompatibleProcessorCount.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Contains all the property, that the CompatibleProcessorCount method needs.
    /// </summary>
    public class CompatibleProcessorCount
    {
        /// <summary>
        /// Gets or Sets the name of the motherboard.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the amount of processors, which is compatible with it.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">the object we want to compare to.</param>
        /// <returns>true, if they are equal.</returns>
        public override bool Equals(object obj)
        {
            CompatibleProcessorCount cpc = obj as CompatibleProcessorCount;
            if (cpc != null)
            {
                return this.Name == cpc.Name &&
                       this.Count == cpc.Count;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode method.
        /// </summary>
        /// <returns>a unique code.</returns>
        public override int GetHashCode()
        {
            return (this.Name.Length * 5) + 110 + (this.Count * 40);
        }
    }
}
