﻿// <copyright file="ManufactureLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;
    using Computers.Repository;

    /// <summary>
    /// This logic class has all the CRUD methods, and the relationship between all the tables are declared here.
    /// </summary>
    public class ManufactureLogic : IManufactureLogic
    {
        private IProcessorRepository processor;
        private IMotherboardRepository motherboard;
        private ICompanyRepository company;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManufactureLogic"/> class.
        /// </summary>
        /// <param name="proc">Repository of the Processor.</param>
        /// <param name="mot">Repository of the Motherboard.</param>
        /// <param name="com">Repository of the Company.</param>
        public ManufactureLogic(IProcessorRepository proc, IMotherboardRepository mot, ICompanyRepository com)
        {
            this.processor = proc;
            this.motherboard = mot;
            this.company = com;
        }

        /// <summary>
        /// method definition which adds a company to the database by the given parameters.
        /// </summary>
        /// <param name="name">The name of the new company.</param>
        /// <param name="language">The language of the new company.</param>
        /// <param name="location">The location where the company is.</param>
        /// <param name="workerCount">The amount of workers in the new company.</param>
        /// <param name="supportEmail">The email which you can contract the new company.</param>
        public void AddCompany(string name, string language, string location, int workerCount, string supportEmail)
        {
            Company entity = new Company
            {
                Name = name,
                Language = language,
                Location = location,
                WorkerCount = workerCount,
                SupportEmail = supportEmail,
            };
            this.company.Insert(entity);
        }

        /// <summary>
        /// method definition which adds a motherboard to the database by the given parameters.
        /// </summary>
        /// <param name="name">The name of the new motherboard.</param>
        /// <param name="socket">The socket which the motherboard has.</param>
        /// <param name="price">The price you have to pay for this motherboard.</param>
        /// <param name="size">The size of the motherboard (mATX, ATX, eATX etc.).</param>
        /// <param name="ramSlots">The amount of RAM slots the motherboard has.</param>
        /// <param name="damaged">A value indicating whether- if the processor is damaged or not.</param>
        /// <param name="companyId">The id of the company which produses this new motherboard.</param>
        public void AddMotherboard(string name, string socket, int price, string size, int ramSlots, bool damaged, int companyId)
        {
            Motherboard entity = new Motherboard
            {
                Name = name,
                Socket = socket,
                Price = price,
                Size = size,
                RamSlots = ramSlots,
                Damaged = damaged,
                CompanyId = companyId,
            };
            this.motherboard.Insert(entity);
        }

        /// <summary>
        /// method definition which adds a processor to the database by the given parameters.
        /// </summary>
        /// <param name="name">The name of the new processor.</param>
        /// <param name="socket">The socket of the new processor.</param>
        /// <param name="price">The price you have to pay for this processor.</param>
        /// <param name="ghz">The maximum Ghz the new processor runs at.</param>
        /// <param name="coreCount">The amount of cores the new processor has.</param>
        /// <param name="damaged">A value indicating whether- if the processor is damaged or not.</param>
        /// <param name="companyId">The id of the company which produses this new processor.</param>
        public void AddProcessor(string name, string socket, int price, double ghz, int coreCount, bool damaged, int companyId)
        {
            Processor entity = new Processor
            {
                Name = name,
                Socket = socket,
                Price = price,
                Ghz = ghz,
                CoreCount = coreCount,
                Damaged = damaged,
                CompanyId = companyId,
            };
            this.processor.Insert(entity);
        }

        /// <summary>
        /// method which changes the location of the company by the given id.
        /// </summary>
        /// <param name="id">a company's id.</param>
        /// <param name="newLocation">the new lotacion which will be set.</param>
        public void ChangeCompanyLocation(int id, string newLocation)
        {
            this.company.ChangeLocation(id, newLocation);
        }

        /// <summary>
        /// method which changes the price of the processor by the given id.
        /// </summary>
        /// <param name="id">a processor's id.</param>
        /// <param name="newPrice">the new price which will be set.</param>
        public void ChangeProcessorPrice(int id, int newPrice)
        {
            var actualproc = this.processor.GetOne(id);
            if (actualproc != null)
            {
                actualproc.Price = newPrice;
            }
            else
            {
               throw new InvalidOperationException("One of the objects does not exist");
            }
        }

        /// <summary>
        /// method definition which changes a processor by the given id.
        /// </summary>
        /// <param name="id">id of the processor.</param>
        /// <param name="socket">socket of the processor.</param>
        /// <param name="damaged">damage of the processor.</param>
        /// <param name="ghz">ghz of the processor.</param>
        /// <param name="coreCount">Core count of the processor.</param>
        /// <param name="name">Name of the processor.</param>
        /// <param name="companyId">Company ID of the processor.</param>
        /// <param name = "price" > the price of the processor.</param>
        /// <returns>true if it can be changed.</returns>
        public bool ChangeProcessor(int id, string socket, bool damaged, double ghz, int coreCount, string name, int companyId, int price)
        {
            return this.processor.ChangeProcessor(id, socket, damaged, ghz, coreCount, name, companyId, price);
        }

        /// <summary>
        /// method which returns a list with all the companies in the database.
        /// </summary>
        /// <returns>Company IList.</returns>
        public IList<Company> GetAllCompany()
        {
            return this.company.GetAll().ToList();
        }

        /// <summary>
        /// method which returns one of the companies by the given id.
        /// </summary>
        /// <returns>Company entity.</returns>
        /// <param name="id">an id of the company.</param>
        public Company GetOneCompany(int id)
        {
            return this.company.GetOne(id);
        }

        /// <summary>
        /// method which returns a list with all the motherboards in the database.
        /// </summary>
        /// <returns>Motherboard IList.</returns>
        public IList<Motherboard> GetAllMotherboard()
        {
            return this.motherboard.GetAll().ToList();
        }

        /// <summary>
        /// method which returns a list with all the processors in the database.
        /// </summary>
        /// <returns>Processor IList.</returns>
        public IList<Processor> GetAllProcessor()
        {
            return this.processor.GetAll().ToList();
        }

        /// <summary>
        /// method which returns one of the motherboards by the given id.
        /// </summary>
        /// <returns>Motherboard entity.</returns>
        /// <param name="id">an id of the motherboard.</param>
        public Motherboard GetOneMotherboard(int id)
        {
            return this.motherboard.GetOne(id);
        }

        /// <summary>
        /// method which returns one of the processors by the given id.
        /// </summary>
        /// <returns>Processor entity.</returns>
        /// <param name="id">an id of the processor.</param>
        public Processor GetOneProcessor(int id)
        {
            return this.processor.GetOne(id);
        }

        /// <summary>
        /// method which removes a company from the database by the given id.
        /// </summary>
        /// <param name="entity">an entity of a company.</param>
        public void RemoveCompany(Company entity)
        {
            this.company.Remove(entity);
        }

        /// <summary>
        /// method which removes a motherboard from the database by the given id.
        /// </summary>
        /// <param name="entity">an entity of a motherboard.</param>
        public void RemoveMotherboard(Motherboard entity)
        {
            this.motherboard.Remove(entity);
        }

        /// <summary>
        /// method which removes a processor from the database by the given id.
        /// </summary>
        /// <param name="entity">an entity of a processor.</param>
        /// <returns>if it can be removed, or not.</returns>
        public bool RemoveProcessor(Processor entity)
        {
            return this.processor.Remove(entity);
        }

        /// <summary>
        /// Displays every motherboard's company, and it's workers.
        /// </summary>
        /// <returns>the workercount of every company.</returns>
        public IList<CompanyWorkerCount> CompanyWorkerCount()
        {
            var q2 = from x in this.motherboard.GetAll()
                     join y in this.company.GetAll()
                     on x.CompanyId equals y.CompanyId
                     select new CompanyWorkerCount
                     {
                         Company = y.Name,
                         Name = x.Name,
                         Size = x.Size,
                         Workers = y.WorkerCount,
                     };
            return q2.ToList();
        }

        /// <summary>
        /// Avarages the processor prices out by it's manufacturer.
        /// </summary>
        /// <returns>A queryable.</returns>
        public IList<AvarageProcessorCost> AvarageProcessorCost()
        {
            var q3 = from x in this.processor.GetAll()
                     join y in this.company.GetAll()
                     on x.CompanyId equals y.CompanyId
                     group x by y.Name into g
                     select new AvarageProcessorCost()
                     {
                         Name = g.Key,
                         Avarage = Math.Round(g.Average(a => a.Price)),
                     };

            return q3.ToList();
        }

        /// <summary>
        /// Async version of the AvarageProcessorCost method.
        /// </summary>
        /// <returns>an IList Task collection of the Avarage Processor costs.</returns>
        public Task<IList<AvarageProcessorCost>> ASyncAvarageProcessorCost()
        {
            return Task<IList<AvarageProcessorCost>>.Factory.StartNew(() =>
            {
                return this.AvarageProcessorCost();
            });
        }

        /// <summary>
        /// Async version of the CompanyWorkerCount method.
        /// </summary>
        /// <returns>an IList Task collection of the Company worker counts.</returns>
        public Task<IList<CompanyWorkerCount>> ASyncCompanyWorkerCount()
        {
            return Task<IList<CompanyWorkerCount>>.Factory.StartNew(() =>
            {
                return this.CompanyWorkerCount();
            });
        }
    }
}
