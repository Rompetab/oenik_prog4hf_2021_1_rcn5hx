﻿// <copyright file="AssembleLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using Computers.Data;
    using Computers.Repository;

    /// <summary>
    /// This logic class makes sure, that the two elements are compatible, and well functioning with each other.
    /// </summary>
    public class AssembleLogic : IAssembleLogic
    {
        private IProcessorRepository processor;
        private IMotherboardRepository motherboard;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssembleLogic"/> class.
        /// </summary>
        /// <param name="proc">Repository of the Processor.</param>
        /// <param name="mot">Repository if the Motherboard.</param>
        public AssembleLogic(IProcessorRepository proc, IMotherboardRepository mot)
        {
            this.processor = proc;
            this.motherboard = mot;
        }

        /// <summary>
        /// This method makes sure if the given processor and motherboard are compatible.
        /// </summary>
        /// <param name="processorId">the id of the processor.</param>
        /// <param name="motherboardId">the id of the motherboard.</param>
        /// <returns>true of they are compatible.</returns>
        public bool Compatible(int processorId, int motherboardId)
        {
            var proc = this.processor.GetOne(processorId);
            var mot = this.motherboard.GetOne(motherboardId);
            if (proc != null && mot != null)
            {
                if (proc.Socket == mot.Socket)
                {
                    return true;
                }

                return false;
            }

            throw new InvalidOperationException("One of the objects does not exist");
        }

        /// <summary>
        /// Gets all the motherboard in the table.
        /// </summary>
        /// <returns>returns a list with all the motherboards in it.</returns>
        public IList<Motherboard> GetAllMotherboard()
        {
            return this.motherboard.GetAll().ToList();
        }

        /// <summary>
        /// Gets all the processor in the table.
        /// </summary>
        /// <returns>returns a list with all the processors in it.</returns>
        public IList<Processor> GetAllProcessor()
        {
            return this.processor.GetAll().ToList();
        }

        /// <summary>
        /// Gets one motherboard back.
        /// </summary>
        /// <returns>returns one motherboard entity.</returns>
        /// <param name="id">the id of the motherboard.</param>
        public Motherboard GetOneMotherboard(int id)
        {
            return this.motherboard.GetOne(id);
        }

        /// <summary>
        /// Gets one processor back.
        /// </summary>
        /// <returns>returns one processor entity.</returns>
        /// <param name="id">the id of the processor.</param>
        public Processor GetOneProcessor(int id)
        {
            return this.processor.GetOne(id);
        }

        /// <summary>
        /// checks if the processor can be overclocked.
        /// </summary>
        /// <param name="id"> the id of the processor.</param>
        /// <returns>true if it can be overclocked.</returns>
        public bool OverclockProcessor(int id)
        {
            var proc = this.processor.GetOne(id);
            if (proc != null)
            {
                string companyProcessorName = proc.Company.Name;
                string processorName = proc.Name;
                if (companyProcessorName == "AMD" || processorName.ToUpper().Contains('K'))
                {
                    return true;
                }

                return false;
            }

            throw new InvalidOperationException("This object does not exist");
        }

        /// <summary>
        /// checks if the motherboard has to be repaired.
        /// </summary>
        /// <param name="id">the id of the motherboard.</param>
        /// <returns>true, if it can be repaired.</returns>
        public bool RepairMotherboard(int id)
        {
            bool motherboardDamage = this.motherboard.GetOne(id).Damaged;
            if (motherboardDamage)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// checks if the processor has to be repaired.
        /// </summary>
        /// <param name="id">the id of the processor.</param>
        /// <returns>true, if it can be repaired.</returns>
        public bool RepairProcessor(int id)
        {
            bool processorDamage = this.processor.GetOne(id).Damaged;
            if (processorDamage)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Changes the processor's name.
        /// </summary>
        /// <param name="id">the id of the processor.</param>
        /// <param name="newName">the new name, which will be given to the processor.</param>
        public void ChangeProcessorName(int id, string newName)
        {
            this.processor.ChangeName(id, newName);
        }

        /// <summary>
        /// Changes the motherboard's socket.
        /// </summary>
        /// <param name="id">the id of the motherboard.</param>
        /// <param name="newSocket">the new socket, which will be set to the motherboard.</param>
        public void ChangeMotherboardSocket(int id, string newSocket)
        {
            this.motherboard.ChangeSocket(id, newSocket);
        }

        /// <summary>
        /// Async version of the CompatibleProcessorCount method.
        /// </summary>
        /// <returns>The compatible motherboards.</returns>
        public IList<CompatibleProcessorCount> CompatibleProcessorCount()
        {
            var q0 = from x in this.motherboard.GetAll()
                     join y in this.processor.GetAll()
                     on x.Socket equals y.Socket
                     group x by x.Name into g
                     select new CompatibleProcessorCount()
                     {
                         Name = g.Key,
                         Count = g.Count(),
                     };

            return q0.ToList();
        }

        /// <summary>
        /// Gives back the motherboards which is compatible with the given processor.
        /// </summary>
        /// <returns>an IList Task collection of the Compatible Processor counts.</returns>
        public Task<IList<CompatibleProcessorCount>> ASyncCompatibleProcessorCount()
        {
            return Task<IList<CompatibleProcessorCount>>.Factory.StartNew(() =>
            {
                return this.CompatibleProcessorCount();
            });
        }
    }
}
