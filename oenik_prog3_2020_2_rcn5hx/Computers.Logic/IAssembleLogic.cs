﻿// <copyright file="IAssembleLogic.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Computers.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Computers.Data;
    using Computers.Repository;

    /// <summary>
    /// A interface which describes the reliationship between motherboard, and processor.
    /// </summary>
    public interface IAssembleLogic : IComputersLogic
    {
        /// <summary>
        /// method definition used for deciding if the two entity is compatible.
        /// </summary>
        /// <param name="processorId">an id of a processor.</param>
        /// <param name="motherboardId">an id of a motherboard.</param>
        /// <returns>true if a processor and a motherboard are compatible.</returns>
        bool Compatible(int processorId, int motherboardId);

        /// <summary>
        /// method definition used for deciding if a processor has to be repaired.
        /// </summary>
        /// <param name="id">an id of a processor.</param>
        /// <returns>true if it has be to repaired.</returns>
        bool RepairProcessor(int id);

        /// <summary>
        /// method definition used for deciding if a motherboard has to be repaired.
        /// </summary>
        /// <param name="id">an id of a motherboard.</param>
        /// <returns>true if it has be to repaired.</returns>
        bool RepairMotherboard(int id);

        /// <summary>
        /// method definition used for deciding if a processor can be overclocked.
        /// </summary>
        /// <param name="id">an id of a processor.</param>
        /// <returns>true if it can be overclocked.</returns>
        bool OverclockProcessor(int id);

        /// <summary>
        /// method definition used for changing the name of a processor.
        /// </summary>
        /// <param name="id">an id of a processor.</param>
        /// <param name="newName">the new name of the processor.</param>
        public void ChangeProcessorName(int id, string newName);

        /// <summary>
        /// Changes the motherboard's socket.
        /// </summary>
        /// <param name="id">the id of the motherboard.</param>
        /// <param name="newSocket">the new socket, which will be set to the motherboard.</param>
        public void ChangeMotherboardSocket(int id, string newSocket);
    }
}
